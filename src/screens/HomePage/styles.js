import { StyleSheet, Dimensions } from 'react-native';

const styles = StyleSheet.create({
  head: {
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingRight: 48
  },
  latest: {
    marginTop: 16,
    height: Dimensions.get('window').height * 0.28
  },
  horizontal: {
    width: Dimensions.get('window').width,
    backgroundColor: '#FFFFFF',
    paddingHorizontal: 24,
    height: Dimensions.get('window').height * 0.06
  },
  buttons: {
    height: 50,
    flex: 1,
    alignContent: 'center'
  },
  divider: {
    backgroundColor: '#F2F2F2',
    height: 1,
    width: Dimensions.get('window').width
  },
  product: {
    backgroundColor: '#FFFFFF',
    height: Dimensions.get('window').height * 0.3,
    flex: 1
  },
  text: {
    paddingHorizontal: 16,
    color: 'red',
    fontFamily: 'Poppins-Regular',
    marginTop: 5
  },
  textPromo: {
    paddingHorizontal: 16,
    color: '#020202',
    fontFamily: 'Poppins-Regular',
    marginTop: 5,
    fontSize: 16
  },
  promoBar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  }
});

export default styles;
