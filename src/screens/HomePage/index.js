/* eslint-disable no-console */
import React, { useState, useEffect, useRef } from 'react';
import { FlatList, View, Text, TouchableOpacity, Dimensions, ScrollView, LogBox } from 'react-native';
import { Divider } from 'react-native-elements';
import Icons from 'react-native-vector-icons/Ionicons';

import { Header, LatestProductCard, ListNewsCard, HorizontalButton, ProductList } from '../../components';
import getProduct from '../../api/getProduct/index';
import getLatestProduct from '../../api/getLatestProduct/index';
import getFeaturedNews from '../../api/getFeaturedNews/index';

import styles from './styles';

function HomePage(props){
  const { navigation } = props;
  const flatListRef = useRef();
  const [refreshing, setRefreshing] = useState(true);
  const [refreshingUp, setRefreshingUp] = useState(true);
  const [refreshingBottom, setRefreshingBottom] = useState(true);
  const [totalPage, setTotalPage] = useState(0);
  const [selectedId, setSelectedId] = useState(1);
  const [page, setPage] = useState(1);
  const [listPerCategory, setListPerCategory] = useState([]);
  const [latest, setLatest] = useState([]);
  const [News, setNews] = useState([]);
  const [category] = useState([
    {
      id: 1,
      name: 'Advanced Connectivity'
    },
    {
      id: 2,
      name: 'Analytics'
    },
    {
      id: 3,
      name: 'Basic Connectivity'
    },
    {
      id: 4,
      name: 'Cloud'
    },
    {
      id: 5,
      name: 'Internet of Things'
    },
    {
      id: 6,
      name: 'Security'
    }
  ]);

  const toTop = () => {
    // use current
    flatListRef.current.scrollToOffset({ animated: true, offset: 0 });
  };

  useEffect(() => {
    handleLatestProduct();
    handleGetProduct(selectedId, page);
    handleGetFeaturedNews();
    LogBox.ignoreLogs(['VirtualizedLists should never be nested']);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    return () => {
      console.log('-- HomePage components cleanup and unmounted');
    };
  }, []);

  const handleGetProduct = async (kategori, halaman) => {
    try {
      const res = await getProduct(kategori, halaman);
      setListPerCategory(res.data.data.products.map(function(o){
        o.quantity = 0;
        return o;
      }));
      setTotalPage(res.data.data.total_pages);
      setRefreshing(false);
    } catch (e) {
      console.log(e);
    }
  };

  const handleLatestProduct = async () => {
    try {
      const resp = await getLatestProduct();
      setLatest(resp.data.data.map(function(o){
        o.quantity = 0;
        return o;
      }));
      setRefreshingUp(false);
    } catch (e){
      console.log(e);
    }
  };

  const handleGetFeaturedNews = async () => {
    try {
      const response = await getFeaturedNews();
      setNews(response.data.data);
      setRefreshingBottom(false);
    } catch (e){
      console.log(e);
    }
  };

  const handleRefresh = () => {
    handleGetProduct(selectedId, page);
  };

  const handleLoadMore = () => {
    if (totalPage > 1) {
      handleAddProduct(selectedId, page);
    }
  };

  const handleAddProduct = async (kategori, halaman) => {
    halaman = halaman + 1;
    try {
      const res = await getProduct(kategori, halaman);
      let listData = listPerCategory;
      if (res.data.data.products.length === 0) {
        setListPerCategory(listData);
        setRefreshing(false);
        setPage(1);
      } else {
        let data = listData.concat(res.data.data.products);
        setListPerCategory(data);
        setRefreshing(false);
        setPage(halaman);
      }
    } catch (e) {
      setRefreshing(false);
      console.log(e);
    }
  };

  const renderItemLatestProduct = ({ item }) => {
    return (
      <LatestProductCard
        name={item.name}
        banner={item.banner}
        category={item.category.name}
        averageRating={item.averageRating}
        length={latest.length}
        OnPress={() => {
          moveToDetailProductFromLatest({ id: item.id });
        }}
      />
    );
  };

  const renderNews = ({ item }) => {
    return (
      <ListNewsCard
        title={item.title}
        banner={item.thumbnail}
        date={item.createdAt}
        OnPress={() => {
          moveScreenToNewsDetail({ id: item.id });
        }}
      />
    );
  };

  const renderButton = ({ item }) => {
    const color = item.id === selectedId ? '#27214D' : '#938DB5';
    return (
      <HorizontalButton
        name={item.name}
        onPress={() => {
          setListPerCategory([]);
          setSelectedId(item.id);
          handleGetProduct(item.id, 1);
          toTop();
          setRefreshing(true);
        }}
        style={{ color }}
      />
    );
  };

  const renderItemProduct = ({ item }) => {
    return (
      <ProductList
        name={item.name}
        banner={item.banner}
        averageRating={item.averageRating}
        price={item.price}
        OnPress={() => {
          moveToDetailProduct({ id: item.id });
        }}
      />
    );
  };

  const moveToDetailProductFromLatest = ({ id }) => {
    navigation.navigate('DetailScreen', { id: latest.filter(obj => {return obj.id === id;}) });
  };

  const moveToDetailProduct = ({ id }) => {
    navigation.navigate('DetailScreen', { id: listPerCategory.filter(obj => {return obj.id === id;}) });
  };

  const moveScreenToNewsDetail = ({ id }) => {
    navigation.navigate('DetailNewsPage', { id: News.filter(obj => {return obj.id === id;}) });
  };

  return (
    <View style={{ flex: 1, backgroundColor: '#FAFAFC' }}>
      <View style={styles.head}>
        <Header title="Tobiko" subTitle="One Stop B2B Solution by Telkomsel" />
        <TouchableOpacity>
          <Icons name="search" size={30} color={'#020202'} onPress={() => navigation.navigate('SearchPage')}/>
        </TouchableOpacity>
      </View>
      <ScrollView nestedScrollEnabled={true}>
        <View style={styles.latest}>
          <FlatList
            data = {latest}
            showsHorizontalScrollIndicator={false}
            horizontal = {true}
            renderItem={renderItemLatestProduct}
            keyExtractor={(item) => item.id.toString()}
            refreshing={refreshingUp}
            onRefresh={handleLatestProduct}
          />
        </View>

        <View style={styles.horizontal}>
          <View style={styles.buttons}>
            <FlatList
              data = {category}
              showsHorizontalScrollIndicator={false}
              horizontal = {true}
              renderItem={renderButton}
              keyExtractor={(item) => item.id.toString()}
              refreshing={refreshing}
              extraData={selectedId}
            // onRefresh={handleLatestProduct}
            />
          </View>
        </View>

        <Divider style={styles.divider} />

        <View style={styles.product}>
          <FlatList
            nestedScrollEnabled={true}
            ref={flatListRef}
            data = {listPerCategory}
            showsVerticalScrollIndicator={false}
            renderItem={renderItemProduct}
            keyExtractor={(item) => item.id.toString()}
            refreshing={refreshing}
            onRefresh={handleRefresh}
            onEndReached={handleLoadMore}
          />
        </View>

        <View style={{ height: Dimensions.get('window').height * 0.18 }}>
          <View style={styles.promoBar}>
            <Text style={styles.textPromo}>
            Latest Promo
            </Text>
            <Text style={styles.text}
              onPress={() => navigation.navigate('AllNewsPage')}>
              {'See All Promo >>'}
            </Text>
          </View>
          <FlatList
            data = {News}
            showsHorizontalScrollIndicator={false}
            horizontal = {true}
            renderItem={renderNews}
            keyExtractor={(item) => item.id.toString()}
            refreshing={refreshingBottom}
            onRefresh={handleGetFeaturedNews}
          />
        </View>
      </ScrollView>
    </View>
  );
}

export default HomePage;
