/* eslint-disable no-console */
import React, { useEffect, useState } from 'react';
import { View, Text, ScrollView, Image, Dimensions, Alert, TextInput, Modal } from 'react-native';
import { Divider } from 'react-native-elements';
import { Picker } from '@react-native-picker/picker';
import RadioButtonRN from 'radio-buttons-react-native';

import { CustomButton, Gap, Header } from '../../components';
import { getToken, showMessage } from '../../utils';
import { getMerchantCity, postOrderMidtrans, getUserAddress, getShippingData, postOrder, postLoan } from '../../api';

import styles from './styles';

const ConfirmOrderPage = ({ route, navigation }) => {
  const { cityFromProfile, selectedProducts, merchantId, subTotalPrice,
    totalWeight, totalAmount, totalItems, dataProductOrder } = route.params;
  const [token, setToken] = useState('');
  const [myAddress, setMyAddress] = useState({});
  const [name, setName] = useState('');
  const [address, setAddress] = useState('');
  const [phone, setPhone] = useState('');
  const [city, setCity] = useState('');
  const [merchantCity, setMerchantCity] = useState(0);
  const [deliveryFee, setDeliveryFee] = useState(0);
  const [totalPrice, setTotalPrice] = useState(totalAmount);
  const [, setShippingPartnerLabel] = useState('');
  const [shippingPartnerValue, setShippingPartnerValue] = useState('jne');
  const [addressModalVisible, setAddressModalVisible] = useState(false);
  const [paymentModalVisible, setPaymentModalVisible] = useState(false);
  const [paymentMethod, setPaymentMethod] = useState('');

  const [shippingPartner] = useState([
    {
      name: 'jne',
      label: 'JNE'
    },
    {
      name: 'tiki',
      label: 'TIKI'
    },
    {
      name: 'pos',
      label: 'POS'
    }
  ]);

  const [myAddressState, setMyAddressState] = useState({
    name: '',
    address: '',
    phone: '',
    city: '',
    cityId: 0
  });

  const [shippingData, setShippingData] = useState([{
    name: '',
    description: '',
    cost: 0,
    etd: '0'
  }]);

  const [shippingService, setShippingService] = useState({
    name: '',
    description: '',
    cost: 0,
    etd: ''
  });

  const [order, setOrder] = useState({
    merchantId: merchantId,
    total: {
      item: totalItems,
      amount: totalAmount
    },
    dataProduct: dataProductOrder,
    shipping: {
      address: cityFromProfile,
      partner: '',
      service: '',
      weight: totalWeight,
      cost: 0,
      etd: 0
    }
  });

  const paymentOptions = [
    {
      label: 'Bank Transfer'
    },
    {
      label: 'Midtrans'
    }
  ];

  function thousands(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  }

  const calculateTotalPrice = () => {
    let finalTotalPrice = totalAmount + deliveryFee;
    setOrder({
      merchantId: merchantId,
      total: {
        item: totalItems,
        amount: finalTotalPrice
      },
      dataProduct: dataProductOrder,
      shipping: {
        address: (myAddress.cityId === 0) ? cityFromProfile : myAddress.city,
        partner: shippingPartnerValue,
        service: shippingService.description,
        weight: totalWeight,
        cost: shippingService.cost,
        etd: shippingService.etd
      }
    });
    setTotalPrice(finalTotalPrice);
  };

  const calculateDeliveryFee = () => {
    setShippingService(shippingData[shippingData.length - 1]);
    setDeliveryFee(shippingService.cost);
  };

  const pickerChange = (index) => {
    shippingPartner.map((itemValue, itemIndex) => {
      if (index === itemIndex) {
        setShippingPartnerLabel(shippingPartner[index].label);
        setShippingPartnerValue(shippingPartner[index].name);
      }
    });
    handleShippingAPI();
    calculateDeliveryFee();
    calculateTotalPrice();
  };

  const handleAddress = async () => {
    const myToken = await getToken();
    setToken(myToken);
    const getAddr = await getUserAddress(myToken);
    // eslint-disable-next-line prefer-destructuring
    const cityId = getAddr.cityId;
    const cityString = getAddr.city.name;
    setMyAddressState({
      name: getAddr.name,
      phone: getAddr.phone,
      address: getAddr.address,
      city: cityString,
      cityId: cityId
    });
  };

  const handleShippingAPI = async () => {
    try {
      const getShippingDataFromAPI = await getShippingData(merchantCity, myAddress.cityId, totalWeight, shippingPartnerValue);
      let shippingDataTemp = [];
      getShippingDataFromAPI.forEach(item => {
        shippingDataTemp.push(item);
      });
      setShippingData(shippingDataTemp);
    } catch (err) {
      console.log(err);
    }
  };

  const handleMerchantCity = async () => {
    const getMerchantCityFromAPI = await getMerchantCity(merchantId);
    setMerchantCity(getMerchantCityFromAPI);
  };

  const handleApplyLoan = () => {
    Alert.alert(
      'Submit Loan',
      'Confirm to apply loan for this order?', [{
        text: 'Cancel',
        style: 'cancel'
      }, {
        text: 'OK',
        onPress: () => {
          onSubmitLoan();
        }
      }], {
        cancelable: false
      }
    );
    return true;
  };

  const onSubmitLoan = async () => {
    if (order) {
      await postOrder(token, order).then( async (response) => {
        let getOrderIdFromAPI = response.data.data.id;
        try {
          const responseLoan = await postLoan(token, getOrderIdFromAPI, totalPrice);
          if (responseLoan.status === 200) {
            showMessage(responseLoan.data.message, 'success');
            navigation.navigate('SuccessOrderLoan');
          }
        } catch (err) {
          showMessage('Submit Loan is Failed', 'danger');
          console.log('error loan: ' + err);
        }

      }).catch((err) => {
        showMessage('Submit Order is Failed:', 'danger');
        console.log('error order via loan: ' + err);
      });
    }
  };

  const submitOrder = async () => {
    await postOrder(token, order).then( async (response) => {
      showMessage(response.data.message, 'success');

      if (paymentMethod === 'TransferPayment') {
        navigation.replace('TransferPayment', { id: response.data.data.id, value: response.data.data.totalAmount, paymentType: 'order' });
        setPaymentModalVisible(!paymentModalVisible);
      }
      else {
        try {
          const myToken = await getToken();
          let res = await postOrderMidtrans(response.data.data.id, response.data.data.totalAmount, myToken);
          navigation.replace('MidTransPayment', { url: res.data.data.payment_link });
          setPaymentModalVisible(!paymentModalVisible);
        } catch (err) {
          // eslint-disable-next-line no-alert
          alert(err.message);
        }
      }
    }).catch((err) => {
      showMessage('Submit Order is Failed', 'danger');
      console.log('error order: ' + err);
    });
  };

  const editDeliveryAddress = (editName, editPhone, editAddress, editCity) => {
    const newAddress = {
      name: editName ? editName : myAddress.name,
      phone: editPhone ? editPhone : myAddress.phone,
      address: editAddress ? editAddress : myAddress.address,
      city: editCity ? editCity : myAddress.city,
      cityId: myAddress.cityId
    };
    setMyAddress(newAddress);
  };

  useEffect(() => {
    handleAddress();
    handleMerchantCity();
    handleShippingAPI();
    calculateDeliveryFee();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (myAddressState) {
      setMyAddress({
        name: myAddressState.name,
        phone: myAddressState.phone,
        address: myAddressState.address,
        city: myAddressState.city,
        cityId: myAddressState.cityId
      });
    }
    calculateDeliveryFee();
    calculateTotalPrice();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [myAddressState, shippingPartnerValue, shippingService, shippingData, deliveryFee]);

  useEffect(() => {
    return () => {
      console.log('-- ConfirmOrderPage components cleanup and unmounted');
    };
  }, []);

  // temp: for debug purposes
  // console.log('outer part:');
  // console.log(shippingData);

  return (
    <View style={styles.page}>
      <Header title="Order Confirmation" subTitle="Only one step more to get the products" onBack={() => navigation.goBack()} />
      <View style={styles.container}>
        <View style={{ flex: 2, justifyContent: 'space-between' }}>
          <ScrollView style={{ flex: 1 }} showsVerticalScrollIndicator={false} >
            <View style={{ marginTop: 16 }}>
              <Text style={styles.section}>Item(s) Ordered</Text>
              {
                selectedProducts.map((item, index) => {
                  return (
                    <View key={index}>
                      <View style={[styles.rowCenterBetween, { marginTop: 16 }]}>
                        <View style={styles.rowCenter}>
                          <View>
                            <Image source={{ uri: item.banner }} style={styles.image}/>
                          </View>
                          <View style={{ marginLeft: 16 }}>
                            <Text style={styles.orderItem}>
                              {item.product}
                            </Text>
                            <Text style={styles.subOrderItem}>{item.qty}{' item(s)'}</Text>
                            <Text style={[styles.subOrderItem, { fontStyle: 'italic' }]}>{'Price: Rp. '}{thousands(item.price)}{',- /item'}</Text>
                          </View>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                          <Text style={styles.subTotalItem}>
                            {'Rp. '}{thousands(item.total)}{',-'}
                          </Text>
                        </View>
                      </View>
                    </View>
                  );
                })
              }
            </View>
            <View style={{ marginTop: 24 }}>
              <Text style={styles.section}>Details Transaction</Text>
              <View style={[styles.rowCenterBetween, { marginTop: 8 }]}>
                <Text style={styles.subSection}>{'Sub-Total Price (Cart)'}</Text>
                <Text style={styles.section}>{'Rp. '}{thousands(subTotalPrice)}{',-'}</Text>
              </View>
              <View style={[styles.rowCenterBetween, { marginTop: 8 }]}>
                <Text style={styles.subSection}>{'Premium Delivery Fee'}</Text>
                <Text style={styles.section}>{'Rp. '}{thousands(deliveryFee)}{',-'}</Text>
              </View>
            </View>

            <View style={{ marginTop: 24 }}>
              <View style={styles.rowCenterBetween}>
                <Text style={styles.section}>Deliver to</Text>
              </View>
              <View style={[styles.rowCenterBetween, { marginTop: 8 }]}>
                <Text style={styles.subSection}>{'Name'}</Text>
                <Text style={styles.subTotalItem}>{myAddress.name}</Text>
              </View>
              <View style={styles.rowCenterBetween}>
                <Text style={styles.subSection}>{'Phone No.'}</Text>
                <Text style={styles.subTotalItem}>{myAddress.phone}</Text>
              </View>
              <View style={styles.rowCenterBetween}>
                <Text style={styles.subSection}>{'Address'}</Text>
                <Text style={styles.subTotalItem}>{myAddress.address}</Text>
              </View>
              <View style={styles.rowCenterBetween}>
                <Text style={styles.subSection}>{'City'}</Text>
                <Text style={styles.subTotalItem}>{myAddress.city}</Text>
              </View>
            </View>

            <View style={{ marginTop: 24 }}>
              <View style={styles.rowCenterBetween}>
                <Text style={styles.section}>Deliver by</Text>
              </View>
              <View style={[styles.rowCenterBetween, { marginTop: 8 }]}>
                <Text style={styles.subSection}>{'Shipping Partner'}</Text>
                <View style={{ borderWidth: 1, borderColor: '#020202', borderRadius: 8, paddingHorizontal: 2, paddingVertical: 0 }}>
                  <Picker mode="dropdown" style={styles.shippingPicker}
                    selectedValue = {shippingPartnerValue}
                    onValueChange = {(itemValue, itemIndex) => pickerChange(itemIndex)} >
                    {
                      shippingPartner.map((item,index) => {
                        return (<Picker.Item label={item.label} value={item.name} key={index} />);
                      })
                    }
                  </Picker>
                </View>
              </View>
            </View>
          </ScrollView>
          <View>
            <Divider style={styles.divider} />
            <View style={[styles.rowCenterBetween, { marginTop: 8, marginBottom: 16 }]}>
              <Text style={styles.section}>{'TOTAL PRICE'}</Text>
              <Text style={styles.subTotalPrice}>{'Rp. '}{thousands(totalPrice)}{',-'}</Text>
            </View>
          </View>
        </View>

        <Modal animationType="slide" parent={true}
          visible={addressModalVisible}
          onRequestClose={() => {
            setAddressModalVisible(!addressModalVisible);
          }}
          onDismiss={() => setAddressModalVisible(!addressModalVisible)}>
          <View style={{ justifyContent: 'flex-end' }}>
            <View style={styles.addressModal}>
              <View style={styles.addressModalContainer}>
                <CustomButton onPress={() => setAddressModalVisible(!addressModalVisible)} width={40} height={40} text="X"/>
              </View>
              <View style={{ marginHorizontal: 30 }}>
                <Text>Deliver to</Text>
                <Gap height={10} width={Dimensions.get('window').width}/>
                <View style={styles.rowCenterBetween}>
                  <Text style={styles.subSection}>{'Name'}</Text>
                  <TextInput style={styles.editAddressTextInput} placeholder={myAddress.name}
                    onChangeText={text => text ? setName(text) : setName(myAddress.name)}
                  />
                </View>
                <Gap height={5} width={Dimensions.get('window').width}/>
                <View style={styles.rowCenterBetween}>
                  <Text style={styles.subSection}>{'Phone No.'}</Text>
                  <TextInput style={styles.editAddressTextInput} placeholder={myAddress.phone}
                    onChangeText={text => text ? setPhone(text) : setName(myAddress.phone)}
                  />
                </View>
                <Gap height={5} width={Dimensions.get('window').width}/>
                <View style={styles.rowCenterBetween}>
                  <Text style={styles.subSection}>{'Address'}</Text>
                  <TextInput style={styles.editAddressTextInput} placeholder={myAddress.address}
                    onChangeText={text => text ? setAddress(text) : setAddress(myAddress.address)}
                  />
                </View>
                <Gap height={5} width={Dimensions.get('window').width}/>
                <View style={styles.rowCenterBetween}>
                  <Text style={styles.subSection}>{'City'}</Text>
                  <TextInput style={styles.editAddressTextInput} placeholder={myAddress.city}
                    onChangeText={text => text ? setCity(text) : setCity(myAddress.city)}
                  />
                </View>
                <Gap height={20} width={Dimensions.get('window').width}/>
                <View style={{ alignItems: 'center' }}>
                  <CustomButton width={Dimensions.get('window').width * 0.7} text="Save"
                    onPress={() => {
                      editDeliveryAddress(name, phone, address, city);
                      setAddressModalVisible(!addressModalVisible);
                    }}/>
                </View>
              </View>
            </View>
          </View>
        </Modal>
      </View>

      <View style={{ justifyContent: 'space-between', alignItems: 'center' }}>
        <View width={Dimensions.get('window').width} style={{ padding: 24 }}>
          <CustomButton text="PAY NOW"
            color={(deliveryFee === 0 ) ? '#8D92A3' : '#1ABC9C'}
            isDisabled={(deliveryFee === 0 ) ? true : false}
            onPress={() => setPaymentModalVisible(!paymentModalVisible)}
          />
          <Gap height={12} />
          <CustomButton text="Apply Loan"
            color={(deliveryFee === 0 ) ? '#8D92A3' : '#FFA451'}
            isDisabled={(deliveryFee === 0 ) ? true : false}
            onPress={handleApplyLoan}
          />
        </View>
      </View>

      <Modal animationType="slide" transparent={true}
        visible={paymentModalVisible}
        onRequestClose={() => {
          setPaymentModalVisible(!paymentModalVisible);
        }}
        onDismiss={() => setPaymentModalVisible(!paymentModalVisible)}>
        <View style={styles.paymentModal}>
          <View style={styles.paymentModalContainer}>
            <CustomButton onPress={() => setPaymentModalVisible(!paymentModalVisible)} width={40} height={40} text="X"/>
          </View>
          <Gap height={20} width={Dimensions.get('window').width}/>
          <View style={{ marginHorizontal: 30 }}>
            <Text>Choose your payment Method :</Text>
            <RadioButtonRN data={paymentOptions} selectedBtn={(input) => {
              if (input.label === 'Bank Transfer') {
                setPaymentMethod('TransferPayment');
              }
              else if (input.label === 'Midtrans') {
                setPaymentMethod('Midtrans');
              }
            }}
            />
            <Gap height={20} width={Dimensions.get('window').width}/>
            <CustomButton text="Submit" onPress={async () => submitOrder()}/>
          </View>
        </View>
      </Modal>
    </View>
  );
};

export default ConfirmOrderPage;
