/* eslint-disable no-console */
import React, { useState, useEffect } from 'react';
import { SafeAreaView, RefreshControl, View, Image, Text, ScrollView, Dimensions } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { connect } from 'react-redux';

import fetchGetProfile from '../../redux/actions/getProfileAction';
import { CustomButton , GreyGap, WidthButton, Header, Gap } from '../../components/index';

import { showMessage } from '../../utils';
import { getToken } from '../../utils';

import { getLoan } from '../../api';

import styles from './styles';

function ProfilePage(props){
  const { navigation } = props;

  const [detailProfile, setDetailProfile] = useState({});
  const [token, setToken] = useState('');
  const [myWallet, setMyWallet] = useState(null);
  const [myLoan, setMyLoan] = useState(null);
  const [refreshing, setRefreshing] = useState(true);

  useEffect(() => {
    processToken();
    // console.log(detailProfile)
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[]);

  useEffect(() => {
    if (props.getProfileReducer.payload.data){
      setDetailProfile(props.getProfileReducer.payload.data);
      setMyWallet(thousands(props.getProfileReducer.payload.data.cashBalance));
    }
    // console.log(detailProfile)
  }, [props.getProfileReducer.payload.data]);

  useEffect(() => {
    return () => {
      console.log('-- ProfilePage components cleanup and unmounted');
    };
  }, []);

  const handleGetProfile = async (auth) => {
    try {
      props.dispatchGetProfile(auth);
    } catch (e) {
      console.log(e);
    }
  };

  const handleLogout = async () => {
    try {
      await AsyncStorage.removeItem('@token');
      await AsyncStorage.removeItem('@isAdmin');
      navigation.replace('SignIn');
    } catch (error) {
      showMessage('Error Logout', error.response);
    }
  };

  const handleZero = (input) => {
    return input != null ? input : 0;
  };

  const processToken = async () => {
    try {
      const mytoken = await getToken();
      if (mytoken){
        setToken(mytoken);
        handleGetProfile(mytoken);
        handleTotalLoan(mytoken);
        setRefreshing(false);
        console.log(token);
      } else {
        navigation.replace('SignIn');
      }
      // console.log('ini token : ', mytoken);
    } catch (e) {
      console.log(e);
    }
  };

  const handleTotalLoan = async (auth) => {
    try {
      let resp = await getLoan(auth);
      if (resp !== undefined){
        let total = 0;
        let i;
        let datafilter = resp.data.data.filter(obj => {
          if (obj.loan.status === 'APPROVED') {return obj;}
        });
        for (i in datafilter){
          total += datafilter[i].loan.nominal;
        }
        setMyLoan(thousands(total));
        console.log(resp);
      }
    } catch (err) {
      console.log(err.message);
    }
  };

  const handleRefresh = () => {
    console.log(token);
    handleTotalLoan(token);
    handleGetProfile(token);
  };

  function thousands(input) {
    let num = input != null ? input : 0;
    return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  }

  return (
    <SafeAreaView style={{ flex: 1, flexDirection: 'column', alignItems: 'center', backgroundColor: 'white' }}>
      <Header title="Profile" subTitle="Your Profile" onBack={() => props.navigation.replace('HomePage')}/>
      <GreyGap height={16} width={Dimensions.get('window').width} />
          <View style={{ flex: 1, flexDirection: 'column', backgroundColor: 'white', alignSelf: 'stretch' }}>
            <ScrollView
              refreshControl={
                <RefreshControl
                  refreshing={refreshing}
                  onRefresh={() => handleRefresh()}
                />
              }>
              <View style={{ alignItems: 'center', backgroundColor: 'white', alignSelf: 'stretch' }}>
                <Image
                  source={detailProfile.photo ? { uri: detailProfile.photo }
                    : require('../../assets/avatar.jpg')}
                  style={{ height: 120, width: 120, borderRadius: 120 / 2, marginTop: 10 }}
                />
                <Text style={[styles.h3, { marginVertical: 10 }]}>{detailProfile.name}</Text>
                <Text style={[styles.h4]}>{detailProfile.email}</Text>
              </View>
              <Gap height={30}/>
              <Text style={styles.h3}>  Funds</Text>
              <View style={{ flexDirection: 'row', marginTop: 20, justifyContent: 'space-around' }}>
                <View style={styles.container}>
                  <Text style={styles.h3}>My Wallet</Text>
                  <Text style={styles.h3}>Rp. {handleZero(myWallet)}</Text>
                  <CustomButton text="Manage" onPress={()=>props.navigation.navigate('WalletPage')} width={125}/>
                </View>
                <View style={styles.container}>
                  <Text style={styles.h3}>My Loan</Text>
                  <Text style={styles.h3}>Rp. {handleZero(myLoan)}</Text>
                  <CustomButton text="Manage" onPress={()=>props.navigation.navigate('LoanPage')} width={125}/>
                </View>
              </View>
              <Gap height={40}/>
              <WidthButton label="  Edit Profile" iconName="chevron-forward" onPress={() => props.navigation.navigate('EditProfilePage')}/>
              <Gap height={3}/>
              <GreyGap height={3} width={Dimensions.get('window').width} />
              <Gap height={15}/>
              <WidthButton label="  My Orders" iconName="chevron-forward" onPress={() => props.navigation.navigate('MyOrdersPage')}/>
              <Gap height={3}/>
              <GreyGap height={3} width={Dimensions.get('window').width} />
              <Gap height={15}/>
              <WidthButton label="  Log Out" iconName="chevron-forward" onPress={() => handleLogout()}/>
              <Gap height={3}/>
              <GreyGap height={3} width={Dimensions.get('window').width} />
              <Gap height={15}/>
            </ScrollView>
          </View>
    </SafeAreaView>
  );
}

function mapStateToProps(state) {
  return {
    getProfileReducer: state.getProfileReducer
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatchGetProfile: (auth) => dispatch(fetchGetProfile(auth))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfilePage);
