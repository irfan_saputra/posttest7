/* eslint-disable no-console */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from 'react';
import { SafeAreaView, View, Text, Dimensions, FlatList, Modal, ActivityIndicator, Alert } from 'react-native';
import RadioButtonRN from 'radio-buttons-react-native';

import { CustomButton , Gap, GreyGap, Header } from '../../components/index';


import { getLoan, postLoanMidtrans } from '../../api';
import { getToken } from '../../utils';

import styles from './styles';

const LoanPage = (props) => {
  const { navigation } = props;
  const [loanData, setLoanData] = useState([]);
  const [modalVisible, setModalVisible] = useState(false);
  const [paymentMethod, setPaymentMethod] = useState('');
  const [token, setToken] = useState('');
  const [urlMidtrans, setUrlMidtrans] = useState('');
  const [loanId, setLoanId] = useState('');
  const [loanNominal, setLoanNominal] = useState(null);
  const [totalLoan, setTotalLoan] = useState(null);
  const [refreshing, setRefreshing] = useState(true);

  const radioData = [
    {
      label: 'Bank Transfer'
    },
    {
      label: 'Midtrans'
    }
  ];

  useEffect(() => {
    processToken();
  },[]);

  useEffect(() => {
    return () => {
      console.log('-- LoanPage components cleanup and unmounted');
    };
  }, []);


  const processToken = async () => {
    try {
      const mytoken = await getToken();
      if (mytoken){
        handleLoanSummary(mytoken);
        handleTotalLoan(mytoken);
        setToken(mytoken);
        console.log(token);
      } else {
        navigation.replace('SignIn');
      }
    } catch (e) {
      console.log(e);
    }
  };

  const handleTotalLoan = async (auth) => {
    try {
      let resp = await getLoan(auth);
      if (resp !== undefined){
        let total = 0;
        let i;
        let datafilter = resp.data.data.filter(obj => {
          if (obj.loan.status === 'APPROVED') {return obj;}
        });
        for (i in datafilter){
          total += datafilter[i].loan.nominal;
        }
        setTotalLoan(thousands(total));
        setRefreshing(false);
        console.log(resp);
      } else {
        setRefreshing(false);
      }
    } catch (err) {
      console.log(err.message);
    }
  };

  const handleLoanSummary = async (auth) => {
    try {
      let result = await getLoan(auth);
      if (result !== undefined){
        let datafilter = result.data.data.filter(obj => {
          if (obj.loan.status === 'WAITING-APPROVAL' || obj.loan.status === 'APPROVED') {return obj;}
        });
        setLoanData(datafilter);
        setRefreshing(false);
        console.log(result);
      } else {
        setRefreshing(false);
      }
      // console.log(datafilter)
    } catch (err) {
      console.log(err.message);
    }
  };

  const handleMidtransTransaction = async (id, nominal, auth) => {
    try {
      let res = await postLoanMidtrans(id,nominal,auth);
      setUrlMidtrans(res.data.data.payment_link);
      //   console.log(urlMidtrans);
      //   console.log(res.data);
    } catch (err) {
      console.log(err.message);
    }
  };

  const handleBankTransferTransaction = (id, nominal) => {
    setLoanId(id);
    setLoanNominal(nominal);
  };

  const handlePayloanDisabled = (item) => {
    if (item !== 'APPROVED') {return true;}
  };
  const handlePayloanColor = (item) => {
    if (item !== 'APPROVED') {return 'grey';}
  };

  const handleZero = (input) => {
    return input != null ? input : 0;
  };

  function thousands(input) {
    let num = input != null ? input : 0;
    return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  }

  const handleRefresh = () => {
    handleLoanSummary(token);
    handleTotalLoan(token);
  };

  const renderItem = ({ item }) => {
    let str = String(item.loan.dueDate);
    let myLoan = thousands(item.loan.nominal);
    // eslint-disable-next-line prefer-destructuring
    let id = item.loan.id;
    let value = item.loan.nominal;

    return (
      <View style={{ flexDirection: 'column' }}>
        <View style={{ flexDirection: 'row', marginHorizontal: '4%', paddingVertical: '4%' }}>
          <View style={{ flexDirection: 'column', justifyContent: 'space-between' }}>
            <Text style={styles.h3}>{item.loan.status}</Text>
            <Text style={styles.h4}>Tenor {item.loan.tenor} Months</Text>
            <Text style={styles.h4}>Due Date {str.substring(0,10)}</Text>
          </View>
          <View style={{ flex: 1, justifyContent: 'space-between',alignItems: 'flex-end' }}>
            <Text style={styles.h3}>Rp. {myLoan}</Text>
            <CustomButton isDisabled={handlePayloanDisabled(item.loan.status)} color={handlePayloanColor(item.loan.status)}
              text ="PAY LOAN"
              onPress={async() => {
                setModalVisible(!modalVisible);
                handleBankTransferTransaction(id, value);
                await handleMidtransTransaction(id, value, token); }}/>
          </View>
        </View>
        <GreyGap height={3}/>
      </View>
    );
  };

  return (
    <SafeAreaView style={{ flex: 1, flexDirection: 'column', backgroundColor: 'white' }}>
      <Header title="Loan" subTitle="Manage Your Loan Here" onBack={()=>props.navigation.goBack()}/>
      <View style={styles.totalloan}>
        <View style={{ alignItems: 'center' }}>
          <Text style={[styles.h2,{ marginLeft: 13, marginBottom: 15 }]}>TOTAL LOAN</Text>
          <Text style={[styles.h1, { marginLeft: 10, fontWeight: 'bold' }]}> Rp. {handleZero(totalLoan)}</Text>
        </View>
      </View>
      <View style={{ flex: 0.1, flexDirection: 'column', justifyContent: 'space-around', marginTop: 10 }}>
        <Text style={[styles.h2,{ marginLeft: '4%', marginHorizontal: 30 }]}>All Loan</Text>
        <GreyGap height={3}/>
      </View>
      {
        (loanData !== []) ?
          <View style={{ flex: 0.95, backgroundColor: 'white' }}>
            <FlatList
              data={loanData}
              showsVerticalScrollIndicator={false}
              contentContainerStyle={{ flexGrow: 1 }}
              renderItem={renderItem}
              refreshing={refreshing}
              onRefresh={() => handleRefresh()}
              keyExtractor={(_, index) => index.toString()}
            />
          </View>
          : <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <ActivityIndicator size="large" color="#FFA451" />
          </View>
      }
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
          setModalVisible(!modalVisible);
        }}
        onDismiss={() => setModalVisible(!modalVisible)}
      >
        <View style={styles.modal}>
          <View style={styles.modal2}>
            <CustomButton onPress={() => setModalVisible(!modalVisible)} text="X  "/>
          </View>
          <Gap height={20} width={Dimensions.get('window').width}/>
          <View style={{ marginHorizontal: 30 }}>
            <Text>Choose your payment Method :</Text>
            <RadioButtonRN
              data={radioData}
              selectedBtn={(input) => {
                if (input.label === 'Bank Transfer') {
                  setPaymentMethod('TransferPayment');
                }
                if (input.label === 'Midtrans') {
                  setPaymentMethod('Midtrans');
                }
                // console.log(input)
                console.log(paymentMethod);}
              }
            />
            <Gap height={20} width={Dimensions.get('window').width}/>
            <CustomButton text="Submit" onPress={() => {
              if (paymentMethod === 'TransferPayment') {
                props.navigation.navigate('TransferPayment', { id: loanId,value: loanNominal, paymentType: 'loan' });
                setModalVisible(!modalVisible);
              } if (paymentMethod === 'Midtrans') {
                props.navigation.navigate('MidTransPayment', { url: urlMidtrans });
                setModalVisible(!modalVisible);}
            }}
            />
          </View>
        </View>
      </Modal>
    </SafeAreaView>
  );
};



export default LoanPage;
