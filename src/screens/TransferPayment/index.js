/* eslint-disable no-alert */
/* eslint-disable no-console */
import React, { useState, useEffect } from 'react';
import { SafeAreaView, View, Image, Text } from 'react-native';
import { launchImageLibrary } from 'react-native-image-picker';

import { CustomButton , GreyGap, Header, Gap } from '../../components/index';


import { postPayLoanTransfer, postPayOrderTransfer } from '../../api';
import { getToken, showMessage } from '../../utils';

import styles from './styles';

function TransferPayment(props){
  const { navigation } = props;
  const [fileUri, SetFileUri] = useState('');
  const [base64, SetBase64] = useState('');
  const [token, setToken] = useState('');

  useEffect(() => {
    processToken();
    console.log(props.route.params);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[]);

  const processToken = async () => {
    try {
      const mytoken = await getToken();
      if (mytoken){
        setToken(mytoken);
        console.log(token);
      } else {
        navigation.replace('SignIn');
      }
    } catch (e) {
      console.log(e);
    }
  };

  const handlePaymentLoan = async (loanid, nominal, base64, auth) => {
    try {
      res = await postPayLoanTransfer(loanid, nominal, base64, auth);
    } catch (err) {
      console.log(err.message);
    }
  };

  const handlePaymentOrder = async (orderid, nominal, base64, auth) => {
    try {
      res = await postPayOrderTransfer(orderid, nominal, base64, auth);
    } catch (err) {
      console.log(err.message);
    }
  };

  // --------- IMAGE PICKER -------------
  const chooseImage = () => {

    let options = {
      title: 'Select Avatar',
      cameraType: 'front',
      mediaType: 'photo',
      maxWidth: 108,
      maxHeight: 108,
      storageOptions: {
        skipBackup: true,
        path: 'images'
      },
      includeBase64: true
    };
    launchImageLibrary(options, (response) => {
      if (response.didCancel) {
        return;
      }
      SetFileUri(response.uri);
      SetBase64(response);
      console.log('response:',response);
    });
  };

  return (
    <SafeAreaView style={{ flex: 1, flexDirection: 'column', alignItems: 'center', backgroundColor: 'white' }}>
      <Header title="Transfer Payment" subTitle="Upload Your Tranfer Receipt Here" onBack={() => navigation.goBack()}/>
      <GreyGap height={16} />
      <View style={styles.container}>
        <View style={{ alignItems: 'center', backgroundColor: 'white', alignSelf: 'stretch' }}>
          <Text style={[styles.h2, { marginVertical: 10, fontWeight: 'bold' }]}>Your Transaction ID :</Text>
          <Text style={[styles.h3, { marginBottom: 30 }]}>{props.route.params.id}</Text>
          {/* <Text style={[styles.h3, {marginBottom:30}]}>{props.route.params.value}</Text>
                    <Text style={[styles.h3, {marginBottom:30}]}>{props.route.params.paymentType}</Text>
                    <Text style={[styles.h3, {marginBottom:30}]}>{typeof base64}</Text> */}
          <Text style={styles.h3}>Please upload your transfer receipt</Text>
          <Image
            source={fileUri ? { uri: fileUri }  // if clicked a new img
              : null} //else show random
            style={{ height: 120, width: 120, marginTop: 20 }}
          />
          <CustomButton text={'Choose File'} onPress={() => chooseImage()}/>
          <Gap height={40}/>
          <CustomButton text={'SUBMIT'} onPress={async () => {
            if (fileUri !== '' && props.route.params.paymentType === 'loan'){
              await handlePaymentLoan(props.route.params.id, props.route.params.value, base64, token).then(SetFileUri(''));
              navigation.reset({ index: 0,routes: [{ name: 'HomePage' }] });
            }

            if (fileUri !== '' && props.route.params.paymentType === 'order'){
              await handlePaymentOrder(props.route.params.id, props.route.params.value, base64, token).then(SetFileUri(''));
              navigation.reset({ index: 0,routes: [{ name: 'HomePage' }] });
            }
            else if (fileUri === '') {
              showMessage('Please Upload Your Transfer Receipt!','danger');
            }
          }}/>
        </View>
      </View>
    </SafeAreaView>
  );
}

export default TransferPayment;
