import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  h1: {
    fontSize: 22,
    marginBottom: 5
  },
  h2: {
    fontSize: 20,
    marginBottom: 5
  },
  h3: {
    fontSize: 17,
    marginBottom: 5
  },
  h4: {
    fontSize: 14,
    marginBottom: 5
  },
  container: {
    flex: 0.95,
    flexDirection: 'column',
    backgroundColor: 'white',
    alignSelf: 'stretch',
    marginTop: 30
  }
});

export default styles;
