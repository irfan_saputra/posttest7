import React from 'react';
import { WebView } from 'react-native-webview';
// import styles from './styles';

const MidTransPayment = ({ route }) => {
  const { url } = route.params;
  // eslint-disable-next-line no-console
  console.log(url);
  return (
  // <SafeAreaView style={{flex:1, alignItems: 'center'}}>
    <WebView
      style={{ flex: 1, alignItems: 'center', marginTop: 40 }}
      // javaScriptEnabled={true}
      // domStorageEnabled={true}
      scalesPageToFit={false}
      // originWhitelist={["file://"]}
      source={{
        uri: url
      }}
    />
  // </SafeAreaView>
  );
};

export default MidTransPayment;
