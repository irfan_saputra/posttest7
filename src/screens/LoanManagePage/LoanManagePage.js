import React from 'react';
import { View, SafeAreaView, Image } from 'react-native';
import { Text, Title, Subheading, Button, Divider } from 'react-native-paper';
import { TabBar, TabView, SceneMap } from 'react-native-tab-view';

const LoanManagePage = (props) => {

  let { navigation } = props;

  const initialLayout = { width: 400 };

  const [index,setIndex] = React.useState(0);
  const [routes] = React.useState([
    { key: 'loan', title: 'Payment Approval' }
  ]);

  const LoanRoute = () => {
    return (
      <View style={{ flex: 1,flexDirection: 'column' }}>
        <View style={{ marginTop: 15,backgroundColor: '#EEEEEE', flex: 2, flexDirection: 'row' }}>

          <Image source={{ uri: 'https://picsum.photos/200/300' }} style={{ width: 78,height: 78,flex: 3 }}/>
          <View style={{ marginVertical: 5, marginHorizontal: 7,flex: 6, flexDirection: 'column' }}>
            <Text style={{ fontSize: 17,color: '#000000',marginVertical: 2 }}>Payment Loan A</Text>
            <Text style={{ fontSize: 15,color: '#666666',marginVertical: 2 }}>3.000.000</Text>
          </View>
          <Button onPress={()=>HandleLoanPayClick()}
            style={{ marginVertical: 10,color: 'black',backgroundColor: '#ffa500',flex: 2,widht: 70,height: 40 }}
            mode="contained">Manage</Button>


        </View>

        <View style={{ backgroundColor: '#EEEEEE', flex: 2, flexDirection: 'row' }}>

          <Image source={{ uri: 'https://picsum.photos/200/300' }} style={{ width: 78,height: 78,flex: 3 }}/>
          <View style={{ marginVertical: 5, marginHorizontal: 7,flex: 6, flexDirection: 'column' }}>
            <Text style={{ fontSize: 17,color: '#000000',marginVertical: 2 }}>Payment Loan A</Text>
            <Text style={{ fontSize: 15,color: '#666666',marginVertical: 2 }}>3.000.000</Text>
          </View>
          <Button onPress={()=>HandleLoanPayClick()} style={{ color: 'black',backgroundColor: '#ffa500',flex: 2,widht: 70,height: 40,marginVertical: 10 }}
            mode="contained">Manage</Button>


        </View>

        <View style={{ backgroundColor: '#EEEEEE', flex: 2, flexDirection: 'row' }}>

          <Image source={{ uri: 'https://picsum.photos/200/300' }} style={{ width: 78,height: 78,flex: 3 }}/>
          <View style={{ marginVertical: 5, marginHorizontal: 7,flex: 6, flexDirection: 'column' }}>
            <Text style={{ fontSize: 17,color: '#000000',marginVertical: 2 }}>Payment Loan A</Text>
            <Text style={{ fontSize: 15,color: '#666666',marginVertical: 2 }}>3.000.000</Text>
          </View>
          <Button onPress={()=>HandleLoanPayClick()}
            style={{ fontSize: 12,backgroundColor: '#ffa500',flex: 2,widht: 70,height: 40,marginVertical: 10 }} mode="contained">Manage</Button>


        </View>
        <View style={{ flex: 5 }} />
      </View>
    );
  };

  const renderTabBar = propstab => {
    return (
      <TabBar
        {...propstab}
        indicatorStyle={{ backgroundColor: 'yellow' }}
        style={{ backgroundColor: 'white' }}
        renderLabel={({ route }) => (
          <Text style={{ color: '#666666', fontSize: 15 }}>
            {route.title}
          </Text>
        )}
      />
    );
  };

  const HandleLoanPayClick = () => {
    navigation.navigate('LoanDetailPage');
  };

  const renderScene = SceneMap({
    loan: LoanRoute
  });


  return (
    <SafeAreaView style={{ flex: 1,flexDirection: 'column',marginHorizontal: 5,marginVertical: 5 }}>
      <View style={{ flex: 1,flexDirection: 'column',marginHorizontal: 5,marginVertical: 10 }}>
        <Title style={{ fontSize: 30 }}>Administration</Title>
        <Subheading style={{ marginBottom: 20 }}>You are now an admin</Subheading>
        <Divider />
        <TabView renderTabBar={renderTabBar}
          navigationState={{ index, routes }} renderScene={renderScene}
          onIndexChange={setIndex} initialLayout={initialLayout}
          style={{ fontSize: 40 }}/>
      </View>
    </SafeAreaView>
  );

};

export default LoanManagePage;
