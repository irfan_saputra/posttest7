/* eslint-disable no-console */
import React, { useState, useEffect } from 'react';
import { SafeAreaView, View, Image, Dimensions } from 'react-native';
import Icons from 'react-native-vector-icons/Ionicons';
import { launchImageLibrary } from 'react-native-image-picker';

import { Header, CustomButton , GreyGap, CustomTextInput } from '../../components/index';
import { getToken, useForm } from '../../utils';
import { putUserData, postAvatar } from '../../api';

import styles from '../EditProfilePage/styles';

function EditProfilePage(props){

  const [fileUri, SetFileUri] = useState('');
  const [form, setForm] = useForm({
    'name': null,
    'password': null
  });
  const [token, setToken] = useState('');

  useEffect(() => {
    processToken();
    console.log(token);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [token]);

  const processToken = async () => {
    getToken().then((mytoken)=>{
      setToken(mytoken);
      console.log(token);
    });
  };
  // --------- IMAGE PICKER -------------
  const chooseImage = () => {

    let options = {
      title: 'Select Avatar',
      cameraType: 'front',
      mediaType: 'photo',
      maxWidth: 108,
      maxHeight: 108,
      storageOptions: {
        skipBackup: true,
        path: 'images'
      },
      includeBase64: true
    };
    launchImageLibrary(options, (response) => {
      if (response.didCancel) {
        return;
      }
      SetFileUri(response.uri);
      console.log('response:',response);
      postAvatar(token,response);
    });
  };

  return (
    <SafeAreaView style={{ flex: 1, flexDirection: 'column', alignItems: 'center', backgroundColor: 'white' }}>
      <Header title="Edit Profile" subTitle="Edit Your Profile Here" onBack={()=>props.navigation.reset({ index: 0,routes: [{ name: 'ProfilePage' }] })}/>
      <GreyGap height={16} width={Dimensions.get('window').width}/>
      <View style={{ alignItems: 'center' }}>
        <Image
          source={fileUri ? { uri: fileUri } // if clicked a new img
            : require('../../assets/avatar.jpg')} //else show random
          style={{ height: 120, width: 120, borderRadius: 120 / 2, marginTop: 20 }}
        />
        <Icons
          name="add-circle-sharp"
          size={50}
          color={'#FFA451'}
          style={{ position: 'absolute', top: 95, left: 75 }}
          onPress={() => chooseImage()} />
      </View>
      <View style={styles.container}>
        <CustomTextInput label="Full Name" placeholder="Input new full name" value={null} onChangeText={(text) => setForm('name', text)} />
        <CustomTextInput label="New Password" placeholder="Input new password" value={null} onChangeText={(text) => setForm('password', text)}/>
        <View style={{ paddingVertical: 15 }}/>
        <CustomButton text="SUBMIT" onPress={async () => await putUserData(token, form)}/>
      </View>
    </SafeAreaView>
  );
}

export default EditProfilePage;
