import {StyleSheet} from 'react-native'

const styles = StyleSheet.create({
    h1:{
        fontSize: 25,
        marginBottom : 5
    },
    h2:{
        fontSize: 22,
        marginBottom : 5
    },
    h3:{
        fontSize: 19,
        marginBottom : 5
    },
    container:{
        flex:0.7, 
        alignSelf:'stretch',
        justifyContent:'space-around', 
        backgroundColor:'white', 
        marginHorizontal:20,
        paddingTop:15,
        // backgroundColor:'red',
        paddingBottom:100
    }
})

export default styles