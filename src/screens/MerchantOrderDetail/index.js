import axios from 'axios';
import React, { useEffect } from 'react';
import { ScrollView } from 'react-native';
import { StyleSheet, Text, View, Image } from 'react-native';
import Config from 'react-native-config';
import { useDispatch, useSelector } from 'react-redux';

import { CustomButton, Gap, Header } from '../../components';
import { getOrderDetails } from '../../redux/actions';
import { getToken } from '../../utils';

const MerchantOrderDetail = ({ route, navigation }) => {

  const { id, approval } = route.params;
  const dispatch = useDispatch();
  const { orderDetail } = useSelector((state) => state.orderReducer);

  useEffect(() => {
    // eslint-disable-next-line no-console
    console.log('useEffect detail');
    dispatch(getOrderDetails(id));
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleApprove = (id) =>{
    getToken().then((res) => {
      // eslint-disable-next-line no-console
      console.log('id', id);
      axios(Config.BASE_URL + '/api/merchant/accept/' + id, {
        method: 'PUT',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'x-api-key': Config.API_KEY,
          'Authorization': 'Bearer ' + res
        }
      }).then((response) => {
        // eslint-disable-next-line no-console
        console.log(response.data);
        navigation.replace('MerchantHome');
      }).catch((err) => {
        // eslint-disable-next-line no-console
        console.log('Errornya : ',err);
      });
    });
  };
  const handleCancel = (id) =>{
    getToken().then((res) => {
      // eslint-disable-next-line no-console
      console.log('id', id);
      axios(Config.BASE_URL + '/api/merchant/discard/' + id, {
        method: 'PUT',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'x-api-key': Config.API_KEY,
          'Authorization': 'Bearer ' + res
        }
      }).then((response) => {
        // eslint-disable-next-line no-console
        console.log(response.data);
        navigation.replace('MerchantHome');
      }).catch((err) => {
        // eslint-disable-next-line no-console
        console.log('Errornya : ',err);
      });
    });
  };

  return (
    <View style={styles.content}>
      <Header title="Order Details" subTitle="merchant order details" onBack={() => navigation.goBack()} />
      <Gap height={12} />
      <ScrollView>
        <View style={{ backgroundColor: 'white' }}>
          <Text style={styles.titleOrder}>Item Ordered</Text>
          {orderDetail.items ? (orderDetail.items.map(item => {
            return (
              <View style={styles.container} key={item.id}>
                <Image source={{ uri: item.product.banner }} style={styles.image} />
                <View style={styles.content}>
                  <Text style={styles.title}>{item.product.name} </Text>
                  <Text style={styles.price}>IDR {item.price}</Text>
                </View>
                <Text style={styles.items}>{item.quantity} items</Text>
              </View>
            );
          })) : (<Text>Data Kosong</Text>)}
        </View>
      </ScrollView>
      {approval && (
        <View style={styles.row}>
          <CustomButton text="Reject" width={100} color="#D9435E" onPress={() => handleCancel(id)}/>
          <Gap width={30} />
          <CustomButton text="Accept" width={100} onPress={() => handleApprove(id)} />
        </View>
      )}
    </View>
  );
};

export default MerchantOrderDetail;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: 'white',
    paddingVertical: 8,
    alignItems: 'center',
    marginLeft: 20
  },
  image: {
    width: 60,
    height: 60,
    borderRadius: 8,
    overflow: 'hidden',
    marginRight: 12
  },
  content: { flex: 1 },
  title: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    color: '#020202'
  },
  titleOrder: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    color: '#020202',
    marginLeft: 16
  },
  price: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    color: '#8D92A3'
  },
  items: { fontSize: 13, fontFamily: 'Poppins-Regular', color: '#8D92A3', marginRight: 16 },
  date: { fontSize: 10, fontFamily: 'Poppins-Regular', color: '#8D92A3' },
  status: (status) => ({
    fontSize: 10,
    fontFamily: 'Poppins-Regular',
    color: status === 'CANCELLED' ? '#D9435E' : '#1ABC9C'
  }),
  row: { flexDirection: 'row', alignSelf: 'center', marginBottom: 16 },
  dot: {
    width: 3,
    height: 3,
    borderRadius: 3,
    backgroundColor: '#8D92A3',
    marginHorizontal: 4
  }
});
