import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  page: {flex: 1, backgroundColor: '#FAFAFC'},
  container: {
    backgroundColor: 'white',
    paddingHorizontal: 16,
    marginTop: 24,
    flex: 1,
  },
  total: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: 16,
    paddingHorizontal: 24,
  },
  image: {height: 80, width: 80, resizeMode: 'cover', borderRadius: 16},
  rowCenter: {flexDirection: 'row', alignItems: 'center'},
  rowCenterBetween: {flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'},
  orderItem: {fontSize: 16, fontFamily: 'Poppins-Medium', color: '#020202'},
  subOrderName: {fontSize: 13, fontFamily: 'Poppins-Medium', color: '#8D92A3'},
  subOrderItem: {fontSize: 13, fontFamily: 'Poppins-Light', color: '#8D92A3'},
  subTotalItem: {fontSize: 14, fontFamily: 'Poppins-Light', color: '#020202'},
});

export default styles;