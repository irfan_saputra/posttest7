import React, { useState } from 'react';
import { StyleSheet, View } from 'react-native';
import Config from 'react-native-config';
import { useDispatch } from 'react-redux';
import { useEffect } from 'react';
import axios from 'axios';

import { getToken, showMessage } from '../../utils';

import { getOrders } from '../../redux/actions';
import { MerchantTabSection, Gap, MerchantHeader } from '../../components';


const MerchantHome = (props) => {
  const { navigation } = props;
  const dispatch = useDispatch();
  const [uri, setUri] = useState('');

  useEffect(() => {
    dispatch(getOrders());
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    getProfileMerchant();
  }, []);

  const getProfileMerchant = () => {
    getToken().then((res) => {
      axios(Config.BASE_URL + '/api/merchant/profile', {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'x-api-key': Config.API_KEY,
          'Authorization': 'Bearer ' + res
        }
      }).then((response) => {
        setUri(response.data.data.merchants[0].photo);
      }).catch((err) => {
        showMessage(`Error: ${err}`);
      });
    });
  };

  return (
    <View style={styles.page} >
      <MerchantHeader title="Merchant" subTitle="Welcome, you're a merchant"
        onBack={() => navigation.navigate('ProfileMerchant')} url={uri} />
      <Gap height={14} />
      <View style={{ flex: 1 }}>
        <MerchantTabSection style={{ flex: 1 }} />
      </View>
    </View>
  );
};
export default MerchantHome;
const styles = StyleSheet.create({
  page: { flex: 1 },
  scene: {
    flex: 1
  }
});
