import React, { useState, useEffect, useRef } from 'react';
import { FlatList, View, Text } from 'react-native';

import { Header, NewsCard } from '../../components';
import getAllNews from '../../api/getAllNews/index';

import styles from './styles';


function AllNewsPage(props){
  const { navigation } = props;
  const flatListRef = useRef();
  const [refreshing, setRefreshing] = useState(true);
  const [totalPage, setTotalPage] = useState(0);
  const [subTitle, setSubTitle] = useState('');
  const [page, setPage] = useState(1);
  const [News, setNews] = useState([]);

  useEffect(() => {
    handleGetAllNews(page);
    setSubTitle('All Promo');
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleGetAllNews = async (halaman) => {
    try {
      const res = await getAllNews(halaman);
      setNews(res.data.data.news);
      setTotalPage(res.data.data.total_pages);
      setRefreshing(false);
    } catch (e) {
      throw e;
    }
  };

  const handleRefresh = () => {
    handleGetAllNews(1);
  };

  const handleLoadMore = () => {
    if (totalPage > 1) {
      handleAddNews(page);
    }
  };

  const handleAddNews = async (halaman) => {
    halaman = halaman + 1;
    try {
      const res = await getAllNews(halaman);
      let listData = News;
      if (res.data.data.news.length === 0) {
        // alert('No More to Reload');
        setNews(listData);
        setRefreshing(false);
        setPage(1);
      } else {
        let data = listData.concat(res.data.data.news);
        setNews(data);
        setRefreshing(false);
        setPage(halaman);
      }
    } catch (e) {
      setRefreshing(false);
      throw e;
    }
  };

  const renderItemNews = ({ item }) => {
    return (
      <NewsCard
        banner={item.thumbnail}
        // banner={'https://gadgetsquad.id/wp-content/uploads/2018/08/Telkomsel-5G-Experience-Center_5.jpg'}
        title={item.title}
        description={item.content}
        subtitle={item.subtitle}
        date={item.createdAt}
        OnPress={() => {
          moveScreenFromAllNews({ id: item.id });
        }}
      />
    );
  };

  const moveScreenFromAllNews = ({ id }) => {
    navigation.navigate('DetailNewsPage', { id: News.filter(obj => {return obj.id === id;}) });
  };

  return (
    <View style={{ flex: 1 }}>
      <Header title="Tobiko" subTitle="One Stop B2B Solution by Telkomsel" />

      <View style={styles.subtitleBar}>
        <Text style={{ fontFamily: 'Poppins-Regular', fontSize: 16 }}>
          {subTitle}
        </Text>
      </View>

      <View style={styles.container}>
        <View>
          <FlatList
            ref={flatListRef}
            data = {News}
            showsVerticalScrollIndicator={false}
            renderItem={renderItemNews}
            keyExtractor={(item) => item.id.toString()}
            refreshing={refreshing}
            onRefresh={handleRefresh}
            onEndReached={handleLoadMore}
          />
        </View>
      </View>
    </View>
  );
}


export default AllNewsPage;
