/* eslint-disable no-console */
import React, { useState, useEffect } from 'react';
import { SafeAreaView, View, Text, Dimensions, FlatList, Modal, ActivityIndicator, Alert } from 'react-native';

import { CustomButton , CustomTextInput, Gap, GreyGap, Header } from '../../components/index';


import { getToken } from '../../utils';
import { getWallet, getWalletHistory, postTopupWallet } from '../../api';
import { showMessage } from '../../utils';

import styles from './styles';
import { KeyboardAvoidingView } from 'react-native';

const WalletPage = (props) => {
  const { navigation } = props;
  const [walletData, setWalletData] = useState([]);
  const [totalWallet, setTotalWallet] = useState([]);
  const [modalVisible, setModalVisible] = useState(false);
  const [token, setToken] = useState('');
  const [topUpNominal, setTopUpNominal] = useState(null);
  const [refreshing, setRefreshing] = useState(true);

  useEffect(() => {
    processToken();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[]);

  const processToken = async () => {
    try {
      const mytoken = await getToken();
      if (mytoken){
        setToken(mytoken);
        handleWalletHistory(mytoken);
        handleTotalWallet(mytoken);
      } else {
        navigation.replace('SignIn');
      }
    } catch (e) {
      console.log(e);
    }
  };

  const handleWalletHistory = async (auth) => {
    try {
      let res = await getWalletHistory(auth);
      if (res !== undefined){
        let datafilter = res.data.data.filter(obj => {
          if (obj.nominal != null && obj.nominal !== 0) {return obj;}
        });
        setWalletData(datafilter);
        setRefreshing(false);
      } else {
        setRefreshing(true);
        console.log('Refresh failed');
      }
    } catch (err) {
      console.log(err.message);
    }
  };

  const handleTotalWallet = async (auth) => {
    try {
      let res = await getWallet(auth);
      if (res){
        setTotalWallet(res.data.data.cashBalance);
      }
    } catch (err) {
      console.log(err.message);
    }
  };

  const handleTopuplWallet = async (auth, nominal) => {
    try {
      let res = await postTopupWallet(auth, nominal);
      console.log(res);
    } catch (err) {
      console.log(err.message);
    }
  };

  const handleColor = (item) => {
    return item === 'Topup' || item === 'Top Up' ? '#008000' : '#FF0000';
  };

  const handlePlusMin = (item) => {
    return item === 'Topup' || item === 'Top Up' ? '+' : '-';
  };

  function thousands(input) {
    let num = input != null ? input : 0;
    return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  }

  const handleZero = (input) => {
    return input != null ? input : 0;
  };

  const handleRefresh = () => {
    console.log(token);
    handleWalletHistory(token);
    handleTotalWallet(token);
  };

  const renderItem = ({ item }) => {
    let str = String(item.transactionTime);
    let myNominal = thousands(item.nominal);
    let myColor = handleColor(item.type);
    let myColor2 = { color: myColor };
    let plusMin = handlePlusMin(item.type);
    return (
      <View style={{ flexDirection: 'column' }}>
        <View style={styles.container1}>
          <View style={styles.container2}>
            <Text style={styles.h3}>{item.type}</Text>
            <Text style={styles.h3}>{item.description}</Text>
            <Text style={styles.h4}>{str.substring(0,10)}</Text>
          </View>
          <View style={{ flex: 1, justifyContent: 'center',alignItems: 'flex-end' }}>
            <Text style={[styles.h2, myColor2]}>{plusMin} Rp. {myNominal}</Text>
          </View>
        </View>
        <GreyGap height={3}/>
      </View>
    );
  };

  let myTotalWallet = thousands(totalWallet);

  return (
    <KeyboardAvoidingView style={{flex:1}}>
    <SafeAreaView style={{ flex: 1, flexDirection: 'column', backgroundColor: 'white' }}>
      <Header title="Wallet" subTitle="Manage Your Wallet Here" onBack={()=>props.navigation.goBack()}/>
      <View style={styles.totalloan}>
        <View >
          <Text style={[styles.h2,{ marginLeft: 13, marginBottom: 10 }]}>TOTAL WALLET</Text>
          <Text style={[styles.h1, { marginLeft: 10, fontWeight: 'bold' }]}> Rp. {handleZero(myTotalWallet)}</Text>
        </View>
        <View style={{ marginRight: 13 }}>
          <CustomButton text={'Top Up'} height={60} onPress={() => setModalVisible(!modalVisible)}/>
        </View>
      </View>
      <View style={styles.historyContainer}>
        <Text style={[styles.h2,{ marginLeft: '4%', marginHorizontal: 30 }]}>History</Text>
        <GreyGap height={3}/>
      </View>
      {(walletData !== []) ?
        <View style={{ flex: 0.95, backgroundColor: 'white' }}>
          <FlatList
            data={walletData}
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{ flexGrow: 1 }}
            renderItem={renderItem}
            refreshing={refreshing}
            keyExtractor={(_, index) => index.toString()}
            onRefresh={() => handleRefresh()}
          />
        </View>
        : <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <ActivityIndicator size="large" color="#FFA451" />
        </View>
      }
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
          setModalVisible(!modalVisible);
        }}
        onDismiss={() => setModalVisible(!modalVisible)}
      >
        <View style={styles.modal}>
          <View style={styles.modal2}>
            <CustomButton height={35} onPress={() => setModalVisible(!modalVisible)} text="X"/>
          </View>
          <Gap height={10} width={Dimensions.get('window').width}/>
          <View style={{ marginHorizontal: 30, alignContent: 'space-between' }}>
            <CustomTextInput label={'Top Up Nominal'} placeholder={'Please Input Top Up Nominal'} onChangeText={(text) => setTopUpNominal(Number(text))}/>
            <Gap height={20} width={Dimensions.get('window').width}/>
            <CustomButton text="Submit" onPress={async () => {
              if (isNaN(topUpNominal) === true || topUpNominal === null){
                showMessage('Please Input Number for TopUp Nominal', 'danger');
              } else {
                await handleTopuplWallet(token,topUpNominal)
                  .then(setTopUpNominal(null))
                  .then(setModalVisible(!modalVisible));
              }
            }}
            />
          </View>
        </View>
      </Modal>
    </SafeAreaView>
    </KeyboardAvoidingView>
  );
};



export default WalletPage;
