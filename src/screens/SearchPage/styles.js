import { StyleSheet, Dimensions } from 'react-native';

const styles = StyleSheet.create({
  searchBar: {
    height: Dimensions.get('window').height * 0.1,
    backgroundColor: '#FFFFFF',
    paddingHorizontal: 24,
    justifyContent: 'center',
    marginTop: 16
  },
  searchBarInside: {
    flexDirection: 'row',
    backgroundColor: '#F3F1F1',
    borderRadius: 8,
    width: '100%'
  },
  searchBarTextInput: {
    height: 56,
    width: 320,
    fontSize: 14,
    lineHeight: 21,
    paddingLeft: 13
  },
  subTitle: {
    height: Dimensions.get('window').height * 0.05,
    backgroundColor: '#FFFFFF',
    paddingHorizontal: 24,
    justifyContent: 'center' ,
    marginTop: 16
  },
  productList: {
    backgroundColor: '#FFFFFF',
    flex: 1
  }
});

export default styles;
