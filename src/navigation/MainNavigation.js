import React from 'react';
import { StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import FlashMessage from 'react-native-flash-message';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icons from 'react-native-vector-icons/Ionicons';
import { SafeAreaProvider } from 'react-native-safe-area-context';

import AdminMerchant from '../screens/AdminMerchant/AdminMerchant';
import AdminPage from '../screens/AdminPage/AdminPage';
import AdminLoan from '../screens/AdminLoan/AdminLoan';

import { SignIn, SignUp, SignUpAddress, SplashScreen, LoanPage, ProfilePage, EditProfilePage,
  MyBasketPage, ForgotPassword, ConfirmOrderPage, SuccessOrderLoan, MyOrdersPage, SuccessOrder,
  MidTransPayment, TransferPayment, SignUpMerchant, SignUpMerchantAddress, WalletPage, DetailNewsPage,
  HomePage, DetailProduct, SearchPage, AllNewsPage, MerchantOrderDetail, MerchantInputResi, ProfileMerchant, MyOrderDetail } from '../screens';

import AdminPaymentLoan from '../screens/AdminPaymentLoan/AdminPaymentLoan';
import MerchantHome from '../screens/MerchantHome';
import MerchantMain from '../screens/MerchantPage/MerchantMain';
import ManageMerchant from '../screens/ManagePage/ManageMerchant';
import PaymentDetailPage from '../screens/PaymentDetailPage/PaymentDetailPage';
import LoanDetailPage from '../screens/LoanDetailPage/LoanDetailPage';
import LoanPaymentPage from '../screens/LoanPaymentPage/LoanPaymentPage';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

function MyTab() {
  return (
    <Tab.Navigator initialRouteName="Home" screenOptions={{ unmountOnBlur: true }} tabBarOptions={{ activeTintColor: '#FFC700', style: styles.container }}>
      <Tab.Screen
        name="HomePage"
        component={HomePage}
        options={{
          tabBarLabel: '',
          tabBarIcon: ({ color, size }) => (
            <Icons name="home" color={color} size={30} />
          )
        }} />
      <Tab.Screen
        name="MyBasketPage"
        component={MyBasketPage}
        options={{
          tabBarLabel: '',
          tabBarIcon: ({ color, size }) => (
            <Icons name="cart" color={color} size={30} />
          )
        }} />
      <Tab.Screen
        name="Profile"
        component={ProfilePage}
        options={{
          tabBarLabel: '',
          tabBarIcon: ({ color, size }) => (
            <Icons name="person" color={color} size={30} />
          )
        }} />
    </Tab.Navigator>
  );
}

const MainNavigation = () => {
  // const { isLoading } = useSelector((state) => state.globalReducer);
  return (
    <SafeAreaProvider>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="SplashScreen" headerMode="none">
          <Stack.Screen name="SplashScreen" component={SplashScreen} />
          <Stack.Screen name="SignIn" component={SignIn} />
          <Stack.Screen name="SignUp" component={SignUp} />
          <Stack.Screen name="SignUpAddress" component={SignUpAddress} />
          <Stack.Screen name="HomePage" component={MyTab} />
          <Stack.Screen name="DetailScreen" component={DetailProduct} />
          <Stack.Screen name="ProfilePage" component={ProfilePage} />
          <Stack.Screen name="EditProfilePage" component={EditProfilePage} />
          <Stack.Screen name="LoanPage" component={LoanPage} />
          <Stack.Screen name="MyBasketPage" component={MyBasketPage} />
          <Stack.Screen name="TransferPayment" component={TransferPayment} />
          <Stack.Screen name="SearchPage" component={SearchPage} />
          <Stack.Screen name="AdminMerchant" component={AdminMerchant} />
          <Stack.Screen name="AdminPage" component={AdminPage} />
          <Stack.Screen name="AdminLoan" component={AdminLoan} />
          <Stack.Screen name="ConfirmOrderPage" component={ConfirmOrderPage} />
          <Stack.Screen name="ForgotPassword" component={ForgotPassword} />
          <Stack.Screen name="MidTransPayment" component={MidTransPayment} />
          <Stack.Screen name="SuccessOrderLoan" component={SuccessOrderLoan} />
          <Stack.Screen name="MyOrdersPage" component={MyOrdersPage} />
          <Stack.Screen name="SuccessOrder" component={SuccessOrder} />
          <Stack.Screen name="AdminPaymentLoan" component={AdminPaymentLoan} />
          <Stack.Screen name="MerchantHome" component={MerchantHome} />
          <Stack.Screen name="SignUpMerchant" component={SignUpMerchant} />
          <Stack.Screen name="SignUpMerchantAddress" component={SignUpMerchantAddress} />
          <Stack.Screen name="AllNewsPage" component={AllNewsPage} />
          <Stack.Screen name="WalletPage" component={WalletPage} />
          <Stack.Screen name="DetailNewsPage" component={DetailNewsPage}/>
          <Stack.Screen name="MerchantOrderDetail" component={MerchantOrderDetail}/>
          <Stack.Screen name="MerchantInputResi" component={MerchantInputResi}/>
          <Stack.Screen name="ProfileMerchant" component={ProfileMerchant}/>
          <Stack.Screen name="MerchantMain" component={MerchantMain} />
          <Stack.Screen name="ManageMerchant" component={ManageMerchant} />
          <Stack.Screen name="PaymentDetailPage" component={PaymentDetailPage} />
          <Stack.Screen name="LoanDetailPage" component={LoanDetailPage} />
          <Stack.Screen name="LoanPaymentPage" component={LoanPaymentPage} />
          <Stack.Screen name="MyOrderDetail" component={MyOrderDetail} />
        </Stack.Navigator>
        <FlashMessage position="top" />
        {/* {isLoading && <Loading />} */}
      </NavigationContainer>
    </SafeAreaProvider>
  );
};

const styles = StyleSheet.create({
  container: {
    borderTopWidth: 0,
    borderTopColor: 'transparent',
    elevation: 0,
    shadowColor: '#5bc4ff',
    shadowOpacity: 0,
    shadowOffset: {
      height: 0
    },
    shadowRadius: 0,
    height: '8%',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center'
  }
});

export default MainNavigation;
