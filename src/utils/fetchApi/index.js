import axios from 'axios';
import Config from 'react-native-config';

const fetchApi = async(method, auth, path, body) => {
  try {
    const response = await axios({ method,
      url: `${Config.BASE_URL}${path}`,
      headers: { 'x-api-key': `${Config.API_KEY}`,
        'Authorization': `Bearer ${auth}` },
      data: body });
    if (response.status !== 400){
      return response;
    }
  } catch (error){
    throw error;
  }
};

export default fetchApi;
