import axios from 'axios';
import Config from 'react-native-config';

export const postOrderMidtrans = async (orderId, total, auth) => {
  try {
    let response = await axios(Config.BASE_URL + '/api/payment/trx/midtrans', {
      method: 'POST',
      headers: {
        'x-api-key': Config.API_KEY,
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${auth}`
      },
      data: {
        'orderId': orderId,
        'total': total
      }
    });
    if (response.status !== 400){
      return response;
    }
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.response);
  }
};
