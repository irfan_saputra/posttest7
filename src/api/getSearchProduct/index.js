import { fetchProduct, showMessage } from '../../utils/';

//FOR GET SEARCH PRODUCT

async function getSearchProduct(word, page){
  try {
    const result = await fetchProduct('GET','','/api/product',null,page,word,null);
    // console.log(result);
    if (result.data.code === 200){
      return result;
    } else {
      showMessage('Not Found', 'warning');
    }
  } catch (error){
    showMessage('Sorry, products not found', 'warning');
    throw error;
  }
}

export default getSearchProduct;
