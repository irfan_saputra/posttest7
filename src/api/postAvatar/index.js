import axios from 'axios';
import Config from 'react-native-config';

import { showMessage } from '../../utils';

export const postAvatar = async (auth,image64) => {
  // eslint-disable-next-line no-console
  console.log('send avatar', auth);
  // eslint-disable-next-line no-console
  console.log({ 'image': `data:${image64.type};base64,${image64.base64}` });
  axios({
    method: 'post',
    url: `${Config.BASE_URL}/api/user/avatar/`,
    data: { 'image': `data:${image64.type};base64,${image64.base64}` },
    headers: {
      'x-api-key': `${Config.API_KEY}`,
      'Authorization': `Bearer ${auth}`
    }
  }).then(function (response) {
    //handle success
    // eslint-disable-next-line no-console
    console.log('berhasil send avatar :', response);
    showMessage('Edit Avatar Success', 'success');
  }).catch(function (response) {
    //handle error
    // eslint-disable-next-line no-console
    console.log(response);
    showMessage('Edit Avatar Failed', 'danger');
  });
};
