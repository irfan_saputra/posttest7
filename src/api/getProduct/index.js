import { fetchProduct, showMessage } from '../../utils/';

//FOR GET PRODUCT

async function getProduct(category, page){
  try {
    const result = await fetchProduct('GET','','/api/product/category/',category,page,'',null);
    // console.log(result);
    return result;
  } catch (error){
    showMessage(error.message, 'warning');
    throw error;
  }
}

export default getProduct;
