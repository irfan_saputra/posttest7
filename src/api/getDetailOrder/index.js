import { fetchApi, showMessage } from '../../utils/';

//FOR GET DETAIL ORDER

async function getAllOrder(auth,orderid){
  try {
    const result = await fetchApi('GET',auth,`/api/order/detail/${orderid}`,null);
    // console.log(result);
    return result;
  } catch (error){
    showMessage(error.message, 'warning');
    throw error;
  }
}

export default getAllOrder;
