import axios from 'axios';
import Config from 'react-native-config';

export const getWallet = async (auth) => {
  try {
    let response = await axios(Config.BASE_URL + '/api/wallet', {
      method: 'GET',
      headers: {
        'x-api-key': Config.API_KEY,
        'Authorization': `Bearer ${auth}`
      }
    });
    if (response.status !== 400){
      return response;
    }
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.response);
  }
};
