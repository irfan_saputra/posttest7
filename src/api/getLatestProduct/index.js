import { fetchProduct, showMessage } from '../../utils/';

//FOR GET LATEST PRODUCT

async function getLatestProduct(category, page){
  try {
    const result = await fetchProduct('GET','','/api/product/latest','','','',null);
    // console.log(result);
    return result;
  } catch (error){
    showMessage(error.message, 'warning');
    throw error;
  }
}

export default getLatestProduct;
