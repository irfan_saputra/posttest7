import { fetchApi, showMessage } from '../../utils';

//FOR PUT FINISH ORDER

async function putFinishOrder(auth,id){
  try {
    const result = await fetchApi('PUT', auth,`/api/order/finish/${id}`, null);
    // eslint-disable-next-line no-console
    console.log('ini hasilnya : ', result);
    if (result.data.code === 200){
      showMessage(result.data.message, 'success');
      return result;
    } else {
      showMessage(result.data.message, 'warning');
    }
  } catch (error){
    throw error.status;
  }
}

export default putFinishOrder;
