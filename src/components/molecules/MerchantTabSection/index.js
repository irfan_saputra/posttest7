import React from 'react';
import { StyleSheet, Text, View, Dimensions, ScrollView } from 'react-native';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';

import { useDispatch, useSelector } from 'react-redux';

import { useEffect } from 'react';
import { useNavigation } from '@react-navigation/native';

import ItemListOrder from '../ItemListOrder';

import { getOrders } from '../../../redux/actions';



const renderTabBar = props => (
  <TabBar
    {...props}
    indicatorStyle={{ backgroundColor: '#020202', height: 3, width: '15%', marginLeft: '3%' }}
    style={{ backgroundColor: 'white' }}
    renderLabel={({ route, focused, color }) => (
      <Text style={{ fontFamily: 'Poppins-Small', color: focused ? '#020202' : '#8092A3' }}>
        {route.title}
      </Text>
    )}
  />
);

const AllOrder = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const { orders } = useSelector((state) => state.orderReducer);

  useEffect(() => {
    dispatch(getOrders());
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <View style={styles.scene}>
      <ScrollView>
        {orders.map((order) => {
          // eslint-disable-next-line no-lone-blocks
          {
            if (order.workflow.id === 2) {
              return <ItemListOrder
                buttonText="Show Detail"
                key={order.id}
                name={order.customer.name}
                invoice={order.invoiceNumber}
                amount={order.totalAmount}
                onPress={() => navigation.navigate('MerchantOrderDetail', { id: order.id,approval: true })}
              />;
            }
          }
        }
        )}
      </ScrollView>
    </View >
  );
};

const Prepared = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const { orders } = useSelector((state) => state.orderReducer);

  useEffect(() => {
    dispatch(getOrders());
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <View style={styles.scene}>
      <ScrollView>
        {orders.map(order => {
          // eslint-disable-next-line no-lone-blocks
          {
            if (order.workflow.id === 3) {
              return <ItemListOrder
                buttonText="Manage"
                key={order.id}
                name={order.customer.name}
                invoice={order.invoiceNumber}
                amount={order.totalAmount}
                onPress={() => navigation.navigate('MerchantInputResi', { id: order.id })}
              />;
            }
          }
        }
        )}
      </ScrollView>
    </View >
  );
};

const OnDelivery = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const { orders } = useSelector((state) => state.orderReducer);

  useEffect(() => {
    dispatch(getOrders());
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  //console.log('all order:', orders)
  return (
    <View style={styles.scene}>
      <ScrollView>
        {orders.map(order => {
          // eslint-disable-next-line no-lone-blocks
          {
            if (order.workflow.id === 4) {
              return <ItemListOrder
                buttonText="Track Order"
                key={order.id}
                name={order.customer.name}
                invoice={order.invoiceNumber}
                amount={order.totalAmount}
                onPress={() => navigation.navigate('MerchantOrderDetail', { id: order.id, approval: false })}
              />;
            }
          }
        }
        )}
      </ScrollView>
    </View >
  );
};

const Completed = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const { orders } = useSelector((state) => state.orderReducer);

  useEffect(() => {
    dispatch(getOrders());
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <View style={styles.scene}>
      <ScrollView>
        {orders.map((order) => {
          // eslint-disable-next-line no-lone-blocks
          {
            if (order.workflow.id === 5) {
              return <ItemListOrder
                buttonText="Show Detail"
                key={order.id}
                name={order.customer.name}
                invoice={order.invoiceNumber}
                amount={order.totalAmount}
                onPress={() => navigation.navigate('MerchantOrderDetail', { id: order.id, approval: false })}
              />;
            }
          }
        }
        )}
      </ScrollView>
    </View >
  );
};

const initialLayout = { width: Dimensions.get('window').width };

const MerchantTabSection = (props) => {

  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    { key: '1', title: 'All Orders' },
    { key: '2', title: 'Prepared' },
    { key: '3', title: 'On Delivery' },
    { key: '4', title: 'Completed' }
  ]);

  const renderScene = SceneMap({
    1: AllOrder,
    2: Prepared,
    3: OnDelivery,
    4: Completed
  });

  return (
    <TabView renderTabBar={renderTabBar}
      navigationState={{ index, routes }}
      renderScene={renderScene}
      onIndexChange={setIndex}
      initialLayout={initialLayout}
    />
  );
};

export default MerchantTabSection;

const styles = StyleSheet.create({});
