import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import { CustomButton } from '../../atoms';

const ItemListOrder = ({ name, invoice, amount, id, onPress, buttonText, ...props }) => {
//   const navigation = { props };
  return (
    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
      <View style={{ flex: 1, marginLeft: 20, marginTop: 16 }}>
        <Text style={styles.name}>{name}</Text>
        <Text style={styles.inv}>{invoice}</Text>
        <Text style={styles.total}>Total Amount: IDR {amount}</Text>
      </View>
      <View style={{ marginRight: 16, marginTop: 16 }}>
        <CustomButton text={buttonText} onPress={onPress}/>
      </View>
    </View>
  );
};

export default ItemListOrder;


const styles = StyleSheet.create({
  price: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    color: '#8D92A3'
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center'

  },
  inv: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13
  },
  name: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    fontWeight: 'bold'
  },
  total: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    color: '#8D92A3'
  }
});
