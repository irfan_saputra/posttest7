import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';

//import logosignin from '../../../assets/secondpage.png'

const CustomButton = ({ text, color = '#FFA451', textcolor = 'white', onPress, width, height, isDisabled = false }) => {
  return (
    <TouchableOpacity activeOpacity={0.7} onPress={onPress} disabled={isDisabled}>
      <View style={style.container(color, width, height)}>
        <Text style={style.label(textcolor)}>{text}</Text>
      </View>
    </TouchableOpacity>
  );
};
const style = StyleSheet.create({
  container: (color, width, height) => ({
    backgroundColor: color,
    //backgroundColor: '#FFA451',
    padding: 10,
    borderRadius: 8,
    width: width,
    height: height,
    justifyContent: 'center',
    alignContent: 'center'
  }),
  label: (textcolor) => ({
    color: textcolor,
    textAlign: 'center',
    fontFamily: 'Poppins-Light'
  })

});
export default CustomButton;
