import React from 'react';
import { View, TouchableOpacity, Text, Image, Dimensions } from 'react-native';

function OrderProductList(props){
  const { OnPress, banner, name, price, quantity, ...others } = props;
  return (
    <TouchableOpacity style={{ flex: 1,
      width: Dimensions.get('window').width , height:  Dimensions.get('window').height * 0.1,
      backgroundColor: '#FFFFFF',alignContent: 'center', justifyContent: 'center' }}
    onPress={OnPress}
    {...others}>
      <View style={{ paddingHorizontal: 24, flexDirection: 'row' }}>
        <Image
          source={{ uri: banner }}
          style={{ height: 65, width: 65, borderRadius: 8 }}
        />
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', flex: 1 }}>
          <View style={{ marginLeft: 12, alignContent: 'center', justifyContent: 'center' }}>
            <Text style={{ fontSize: 14, fontWeight: '400', color: '#020202', fontFamily: 'Poppins-Medium' }}>
              {name}
            </Text>
            <Text style={{ fontSize: 14, fontWeight: '400', color: '#8D92A3' }}>
              Rp {price}
            </Text>
          </View>
          <Text style={{ fontSize: 14, fontWeight: '400', color: '#8D92A3' }}>
            {quantity} items
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
}

export default OrderProductList;
