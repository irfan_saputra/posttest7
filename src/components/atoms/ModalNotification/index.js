import React from 'react';
import { Modal, TouchableOpacity, Text, View, StyleSheet, Image, Dimensions } from 'react-native';

function ModalNotification(props){
  const { buttonText, title, textColor, buttonColor, content, banner, ...others } = props;

  return (
    <View>
      <Modal
        animationType="slide"
        transparent={true}
        {...others}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>{title}</Text>
            <Text style={styles.modalTextContent}>
              {content}
            </Text>
            <Image
              source={banner ? { uri: banner } : null}
              style={{ width: Dimensions.get('window').width * 0.7 , height: Dimensions.get('window').height * 0.2 , borderRadius: 8, marginTop: 12 }}
            />
            <TouchableOpacity
              style={{ ...styles.openButton, backgroundColor: buttonColor, marginTop: 16 }}
              {...others}
            >
              <Text style={{ ...styles.textStyle, color: textColor }}>{buttonText}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </View>
  );
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22
  },
  modalView: {
    margin: Dimensions.get('window').width * 0.1,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
    color: '#2E3E5C',
    fontSize: 18,
    fontFamily: 'Poppins-Bold',
    fontWeight: 'bold'
  },
  openButton: {
    backgroundColor: '#F194FF',
    borderRadius: 20,
    padding: 10,
    elevation: 2,
    width: 125
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
    fontFamily: 'Poppins-Regular'
  },
  modalTextContent: {
    marginBottom: 15,
    textAlign: 'center',
    color: '#2E3E5C',
    fontSize: 14,
    fontFamily: 'Poppins-Regular'
  }
});

export default ModalNotification;
