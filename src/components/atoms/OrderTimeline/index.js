import React from 'react';
import { StyleSheet, Text, View, Image, Dimensions } from 'react-native';
import Icons from 'react-native-vector-icons/Ionicons';

import { ILLine } from '../../../assets';


export default function OrderTimeline(props) {
  const { id } = props;
  return (
    <View>
      <Text style={styles.section}>Order Timeline</Text>
      <View style={{ flexDirection: 'row', marginTop: 12, width: Dimensions.get('window').width }}>
        <View style={{ height: 80, width: 80, borderRadius: 10, backgroundColor: '#FFFAEB', alignItems: 'center', justifyContent: 'center' }}>
          <Icons name="cart" size={40} color={'#4CD964'}/>
        </View>
        <View style={{ marginLeft: 10, alignItems: 'center', flexDirection: 'row',
          width: Dimensions.get('window').width * 0.7, justifyContent: 'space-between', paddingRight: 16 }}>
          <View>
            <Text style={{ fontSize: 16, fontWeight: 'bold' }}>
                                Order Made
            </Text>
            <Text style={{ fontSize: 12, fontWeight: '500', marginTop: 4 }}>
              {id === 1 || id === 8 ? 'Waiting for Payment' : 'Paid'}
            </Text>
          </View>
          <Icons name="checkmark-circle" size={25} color={'#4CD964'}/>
        </View>
      </View>
      <Image source={ILLine} style={{ height: 40, width: 2, marginLeft: 40 }}/>
      <View style={{ flexDirection: 'row', marginTop: 12 }}>
        <View style={{
          height: 80,
          width: 80,
          borderRadius: 10,
          backgroundColor: id >= 2 && id !== 8 ? '#FFFAEB' : '#F1EFF6', alignItems: 'center', justifyContent: 'center' }}>
          <Icons name="cash" size={40} color={id >= 2 && id !== 8 ? '#4CD964' : '#8D92A3'}/>
        </View>
        <View style={{ marginLeft: 10, alignItems: 'center', flexDirection: 'row',
          width:  Dimensions.get('window').width * 0.7, justifyContent: 'space-between', paddingRight: 16 }}>
          <View>
            <Text style={{ fontSize: 16, fontWeight: 'bold', color: id >= 2 && id !== 8 ? '#020202' : '#8D92A3' }}>
              {id >= 2 ? 'Payment Verified' : 'Waiting for Payment'}
            </Text>
          </View>
          <Icons name="checkmark-circle" size={25} color={id >= 2 && id !== 8 ? '#4CD964' : '#8D92A3'}/>
        </View>
      </View>
      <Image source={ILLine} style={{ height: 40, width: 2, marginLeft: 40 }}/>
      <View style={{ flexDirection: 'row', marginTop: 12 }}>
        <View style={{ height: 80,
          width: 80,
          borderRadius: 10,
          backgroundColor: id >= 3 && id !== 8 && id !== 6 && id !== 7 ? '#FFFAEB' : '#F1EFF6', alignItems: 'center', justifyContent: 'center' }}>
          <Icons name="cube" size={40} color={id >= 3 && id !== 8 && id !== 6 && id !== 7 ? '#4CD964' : '#8D92A3'}/>
        </View>
        <View style={{ marginLeft: 10, alignItems: 'center', flexDirection: 'row',
          width:  Dimensions.get('window').width * 0.7, justifyContent: 'space-between', paddingRight: 16 }}>
          <View>
            <Text style={{ fontSize: 16, fontWeight: 'bold', color: id >= 3 && id !== 8 && id !== 6 && id !== 7 ? '#020202' : '#8D92A3' }}>
                                Prepared by Merchant
            </Text>
          </View>
          <Icons name="checkmark-circle" size={25} color={id >= 3 && id !== 8 && id !== 6 && id !== 7 ? '#4CD964' : '#8D92A3'}/>
        </View>
      </View>
      <Image source={ILLine} style={{ height: 40, width: 2, marginLeft: 40 }}/>
      <View style={{ flexDirection: 'row', marginTop: 12 }}>
        <View style={{
          height: 80,
          width: 80,
          borderRadius: 10,
          backgroundColor: id >= 4 && id !== 8 && id !== 6 && id !== 7 ? '#FFFAEB' : '#F1EFF6', alignItems: 'center', justifyContent: 'center' }}>
          <Icons name="paper-plane" size={40} color={id >= 4 && id !== 8 && id !== 6 && id !== 7 ? '#4CD964' : '#8D92A3'}/>
        </View>
        <View style={{ marginLeft: 10, alignItems: 'center', flexDirection: 'row',
          width:  Dimensions.get('window').width * 0.7, justifyContent: 'space-between', paddingRight: 16 }}>
          <View>
            <Text style={{ fontSize: 16, fontWeight: 'bold', color: id >= 4 && id !== 8 && id !== 6 && id !== 7 ? '#020202' : '#8D92A3' }}>
                                Shipped
            </Text>
          </View>
          <Icons name="checkmark-circle" size={25} color={id >= 4 && id !== 8 && id !== 6 && id !== 7 ? '#4CD964' : '#8D92A3'}/>
        </View>
      </View>
      <Image source={ILLine} style={{ height: 40, width: 2, marginLeft: 40 }}/>
      <View style={{ flexDirection: 'row', marginTop: 12 }}>
        <View style={{
          height: 80,
          width: 80,
          borderRadius: 10,
          backgroundColor: id === 5 ? '#FFFAEB' : '#F1EFF6', alignItems: 'center', justifyContent: 'center' }}>
          <Icons name="logo-dropbox" size={40} color={id === 5 ? '#4CD964' : '#8D92A3'}/>
        </View>
        <View style={{ marginLeft: 10, alignItems: 'center', flexDirection: 'row',
          width:  Dimensions.get('window').width * 0.7, justifyContent: 'space-between', paddingRight: 16 }}>
          <View>
            <Text style={{ fontSize: 16, fontWeight: 'bold', color: id === 5 ? '#020202' : '#8D92A3' }}>
                                Arrived at Destination
            </Text>
          </View>
          <Icons name="checkmark-circle" size={25} color={id === 5 ? '#4CD964' : '#8D92A3'}/>
        </View>
      </View>
      <Image source={ILLine} style={{ height: 40, width: 2, marginLeft: 40 }}/>
      <View style={{ flexDirection: 'row', marginTop: 12 }}>
        <View style={{
          height: 80,
          width: 80,
          borderRadius: 10,
          backgroundColor: (id === 6 || id === 5 || id === 7) ? '#FFFAEB' : '#F1EFF6', alignItems: 'center', justifyContent: 'center' }}>
          <Icons name="checkmark-done-circle" size={40} color={(id === 6 || id === 5 || id === 7) ? '#4CD964' : '#8D92A3'}/>
        </View>
        <View style={{ marginLeft: 10, alignItems: 'center', flexDirection: 'row',
          width:  Dimensions.get('window').width * 0.7, justifyContent: 'space-between', paddingRight: 16 }}>
          {id < 5 || id === 8 ?
            <View>
              <Text style={{ fontSize: 16, fontWeight: 'bold', color: (id === 6 || id === 5 || id === 7) ? '#020202' : '#8D92A3' }}>
                                Finish
              </Text>
            </View>
            : <View>
              <Text style={{ fontSize: 16, fontWeight: 'bold', color: (id === 6 || id === 5 || id === 7) ? '#020202' : '#8D92A3' }}>
                                Finish
              </Text>
              <Text style={{ fontSize: 12, fontWeight: '500', marginTop: 4 }}>
                {id === 5 ? 'Enjoy Our Product' : id === 6 ? 'Cancelled by Customer' : id === 7 ? 'Cancelled by Merchant' : ''}
              </Text>
            </View>
          }
          <Icons name="checkmark-circle" size={25} color={(id === 6 || id === 5 || id === 7) ? '#4CD964' : '#8D92A3'}/>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  section: { fontSize: 14, fontFamily: 'Poppins-Light', color: '#020202', fontWeight: 'bold' }
});

