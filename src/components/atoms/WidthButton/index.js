import React from 'react';
import { View, StyleSheet, TouchableOpacity, Dimensions,Text } from 'react-native';
import Icons from 'react-native-vector-icons/Ionicons';

const WidthButton = ({ label, iconName, onPress }) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={style.container}>
        <Text style={style.label}>{label}</Text>
        <Icons style={style.icon} name={iconName} size={20} color={'black'}/>
      </View>
    </TouchableOpacity>
  );
};

const style = StyleSheet.create({
  container: {
    width: Dimensions.get('window').width,
    flexDirection: 'row'
  },
  label: {
    alignItems: 'flex-start',
    fontSize: 17
  },
  icon: {
    alignSelf: 'flex-end'
  }
});
export default WidthButton;
