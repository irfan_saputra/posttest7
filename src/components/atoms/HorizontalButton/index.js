import React from 'react';
import { View, TouchableOpacity, StyleSheet, Text, Dimensions } from 'react-native';

function HorizontalButton(props){
  const { onPress, name, style, ...others } = props;
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[
        styles.h2,
        style
      ]}>
      <View
        style={styles.h1}>
        <Text
          style={[
            styles.Text,
            style
          ]}
          {...others}>
          {name}
        </Text>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  h1: {
    flex: 1,
    alignContent: 'center',
    justifyContent: 'center'
  },
  h2: {
    height: Dimensions.get('window').height * 0.07,
    backgroundColor: '#FFFFFF',
    marginRight: 24
  },
  Text: {
    lineHeight: 32,
    fontWeight: '500',
    letterSpacing: -0.02,
    fontSize: 14,
    color: '#020202',
    fontFamily: 'Poppins-Medium'
  }
});

export default HorizontalButton;
