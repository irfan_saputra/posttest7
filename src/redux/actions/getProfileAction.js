import { getProfile } from '../../api';

function fetchGetProfile(auth){
  return async (dispatch) => {
    dispatch({
      type: 'FETCH_GETPROFILE_REQUEST'
    });

    try {
      const result = await getProfile(auth);

      if (result.status === 200){
        dispatch({
          type: 'FETCH_GETPROFILE_SUCCESS',
          payload: result.data
        });
      } else {
        dispatch({
          type: 'FETCH_GETPROFILE_FAILED',
          payload: result.data
        });
      }
    } catch (error){
      dispatch({
        type: 'FETCH_GETPROFILE_FAILED',
        error: error
      });
    }
  };
}

export default fetchGetProfile;
