import axios from 'axios';
import Config from 'react-native-config';

import { showMessage } from '../../utils';

import { setLoading } from './global';

export const signUpActionMerchant = (data, navigation) => (dispatch) => {
  // eslint-disable-next-line no-console
  console.log('data di redux', data);

  axios(Config.BASE_URL + '/api/merchant/register', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'x-api-key': Config.API_KEY
    },
    data: data
  }).then((response) => {
    showMessage('Sign Up is Successfully','success');
    //storeData(response.data.data.token);
    dispatch(setLoading(false));
    navigation.replace('SignIn');
  })
    .catch((err) => {
      showMessage('Sign Up is Failed');
      //console.log(err.response.data.message);
      //setLoading(false);
      dispatch(setLoading(false));
    });
};
