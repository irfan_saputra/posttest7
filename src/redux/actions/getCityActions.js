//import getProvince from '../../api'
import axios from 'axios';
import Config from 'react-native-config';

function fetchGetCity(id){
  return async (dispatch) => {
    // eslint-disable-next-line no-console
    console.log('masuk ke fetch get city');
    dispatch({
      type: 'FETCH_GETCITY_REQUEST'
    });

    try {
      const result = await axios(Config.BASE_URL + '/api/data/city/' + id, {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'x-api-key': Config.API_KEY
        }
      }).catch((error) => {
        // eslint-disable-next-line no-console
        console.log('Error di action get City' ,error);
      });

      if (result.status === 200){
        dispatch({
          type: 'FETCH_GETCITY_SUCCESS',
          payload: result.data.data.cities
        });
        //console.log('get city berhasil')
      } else {
        dispatch({
          type: 'FETCH_GETCITY_FAILED',
          payload: result.data.data
        });
      }
    } catch (error){
      // eslint-disable-next-line no-console
      console.log('error', error);
      dispatch({
        type: 'FETCH_GETCITY_FAILED',
        error: error
      });
    }
  };
}

export default fetchGetCity;
