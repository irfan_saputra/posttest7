import axios from 'axios';
import Config from 'react-native-config';

import { showMessage } from '../../utils';

import { setLoading } from './global';

export const signUpAction = (data, navigation) => (dispatch) => {
  // eslint-disable-next-line no-console
  console.log('data di redux', data);
  axios(Config.BASE_URL + '/api/user/register', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'x-api-key': Config.API_KEY
    },
    data: data
  }).then((response) => {
    // eslint-disable-next-line no-console
    console.log(response.data.data);
    showMessage(response.data.data,'success');
    //storeData(response.data.data.token);
    dispatch(setLoading(false));
    navigation.replace('SignIn');
  })
    .catch((err) => {
      showMessage(err.response.data.message);
      // eslint-disable-next-line no-console
      console.log(err);
      //setLoading(false);
      dispatch(setLoading(false));
    });
};
