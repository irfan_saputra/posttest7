//import getProvince from '../../api'
import axios from 'axios';
import Config from 'react-native-config';

function fetchGetProvince(){
  return async (dispatch) => {
    //console.log('masuk ke fetch')
    dispatch({
      type: 'FETCH_GETPROVINCE_REQUEST'
    });

    try {
      const result = await axios(Config.BASE_URL + '/api/data/province', {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'x-api-key': Config.API_KEY
        }
      }).catch((error) => {
        // eslint-disable-next-line no-console
        console.log('Error di action getProvince' ,error);
      });

      if (result.status === 200){
        dispatch({
          type: 'FETCH_GETPROVINCE_SUCCESS',
          payload: result.data.data
        });
        //console.log('get province berhasil')
      } else {
        dispatch({
          type: 'FETCH_GETPROVINCE_FAILED',
          payload: result.data.data
        });
      }
    } catch (error){
      // eslint-disable-next-line no-console
      console.log('error', error);
      dispatch({
        type: 'FETCH_GETPROVINCE_FAILED',
        error: error
      });
    }
  };
}

export default fetchGetProvince;
