const defaultState = {
  payload: {},
  isLoading: false
};

export default (state = defaultState, action = {}) => {
  switch (action.type){
    case 'FETCH_GETCITY_REQUEST': {
      return {
        ...state,
        isLoading: true
      };
    }

    case 'FETCH_GETCITY_SUCCESS': {
      return {
        ...state,
        payload: action.payload,
        isLoading: false
      };
    }

    case 'FETCH_GETCITY_FAILED': {
      return {
        ...state,
        payload: {},
        isLoading: false
      };
    }

    default:
      return state;
  }
};
