// const defaultState = {
//     items : [],
//     addedItems : [],
//     total: 0,
//     isLoading : false,
//     error : {}
// }

// export default (state = defaultState, action = {}) => {
//     switch (action.type){
//         case 'ADD_TO_CART': {
//           let addedItem = state.items.find(item=> item.id === action.id)
//                 //check if the action id exists in the addedItems
//           let existed_item= state.addedItems.find(item=> action.id === item.id)
//           if(existed_item){
//             console.log('ini existed', existed_item)
//             console.log('ini added', addedItem)
//             // addedItem.quantity += 1
//             addedItem.quantity = addedItem.quantity + action.quantity
//             return{
//               ...state,
//               total : state.total + addedItem.price*action.quantity
//             }
//           } else {
//               addedItem.quantity = action.quantity;
//               //calculating the total
//               let newTotal = state.total + addedItem.price*action.quantity
//               return{
//                 ...state,
//                 addedItems: [...state.addedItems, addedItem],
//                 total : newTotal
//               }
//           }

//         }

//         default:
//             return state;
//     }
// }


const defaultState = {
  payload: {},
  isLoading: false,
  totalAmount: 0,
  totalObject: {},
  cartArrObject: [],
  merchantIdDefault: ''
};

export default (state = defaultState, action = {}) => {
  switch (action.type){
    case 'FETCH_GETCART_REQUEST': {
      return {
        ...state,
        isLoading: true
      };
    }

    case 'FETCH_GETCART_SUCCESS': {
      // console.dir(state.totalObject, { depth: null });
      // let isChecked = false;
      // eslint-disable-next-line prefer-destructuring
      let defMerch = action.payload.cart[0];
      return {
        ...state,
        payload: action.payload,
        isLoading: false,
        totalAmount: action.payload.total.amount,
        totalObject: action.payload.total,
        // cartArrObject: [...action.payload.cart, isChecked],
        cartArrObject: action.payload.cart,
        merchantIdDefault: defMerch.merchantId
      };
    }

    case 'FETCH_GETCART_FAILED': {
      return {
        ...state,
        payload: {},
        isLoading: false
      };
    }

    default:
      return state;
  }
};
