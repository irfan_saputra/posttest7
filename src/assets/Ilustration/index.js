import Logo from './Logo.svg';
import IlSuccessSignUp from './SuccessSignUp.svg';
import IlSuccessOrder from './SuccessOrder.svg';
import IlEmptyOrder from './EmptyOrder.svg';
import IlSuccessOrderLoan from './SuccessOrderLoan.png';
import ILLine from './line.png';

export {Logo, IlSuccessSignUp, IlSuccessOrder, IlEmptyOrder, IlSuccessOrderLoan, ILLine};
