import React from 'react';
import { shallow } from 'enzyme';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import CustomTextInput from '../src/components/atoms/CustomTextInput/index';


Enzyme.configure({ adapter: new Adapter() });

describe('Button', () => {
  describe('Rendering', () => {
    it('should match to snapshot', () => {
      const component = shallow(<CustomTextInput label="test label" placeholder ="Test Placeholder" value="Irfan Saputra" />);
      expect(component).toMatchSnapshot();
    });
  });
});