import React from 'react';
import { shallow } from 'enzyme';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import CustomButton from '../src/components/atoms/CustomButton/index';


Enzyme.configure({ adapter: new Adapter() });

describe('Button', () => {
  describe('Rendering', () => {
    it('should match to snapshot', () => {
      const component = shallow(<CustomButton text="test label Button" width={1000} height={1000} />);
      expect(component).toMatchSnapshot();
    });
  });
});
